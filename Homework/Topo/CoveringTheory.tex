\input{/home/flo/Documents/tex/header}
\begin{document}
\section{Summary of Covering Theory}
\begin{definition}
  Let $p:Y \to X$ be a surjective map of topological spaces, then $p$ is called a
  \textit{covering map} if every point $x \in X$ has an open neighborhood $U$
  such that:
  \[ p^{-1}(U)\iso S \times U = \coprod_S U\]
  where $S$ is some discrete set which a priori depends on $x$. The category of
  covering spaces of $X$ denoted $\rm{Cov}_X$ is the category where objects are covering
  maps $Y \to X$ and morphisms are given by commutative diagrams:
\[\begin{tikzcd}
	{Y} && {Y\p} \\
	& {X}
	\arrow[from=1-1, to=2-2]
	\arrow[from=1-3, to=2-2]
	\arrow[from=1-1, to=1-3]
\end{tikzcd}\]
\end{definition}
\noindent
\begin{remark}
\begin{itemize}
  \item In particular $p^{-1}(x)\iso S$ i.e.~$p$ has discrete fibers. Moreover the
    fiber $S$ depends only on the connected component of $x$
    \item $p$ is a homeomorphism locally on $Y$.
\end{itemize}
\end{remark}
\begin{example}
\begin{enumerate}
\item For every set $S$ the \textit{trivial covering} with fiber $S$ is the projection
  map:
  \begin{align*}
    S\times X \to X\\
    (s, x)\mapsto x
  \end{align*}
  Note that another way to state the definition of a covering is to say that
  it's a map that is locally (on the base) of this form (but not necessarily globally.)
\item The map :
  \begin{align*}
    S^1 \to S^1\\
    x \mapsto x^n
  \end{align*}
  is a covering with fiber $\Z/n$. Note that the fiber inherits a natural group
  structure from the group structure on $S^1$ (because the map is multiplicative).
    \item Let $G \act X$ be a free + properly discontinuous group action. Then
      the quotient map:
      \begin{align*}
       X \to X_G\\
        x \mapsto xG
      \end{align*}
      is a covering. Observe that each fiber carries a natural $G$-action which
      is free and transitive, but the fibers themselves do not inherit a group
      structure. 
\end{enumerate}
\end{example}
\begin{definition}
  Let $X$ be any space and $x\in X$ a basepoint. The fundamental group of $X$ is
  the group of pointed homotopy classes: of pointed homotopy classes:
  \[ \pi_1(X, x):= [(S^1, 1), (X, x)]_\pt\]
  equivalently these are pointed homotopy classes of loops $\gamma: [0,1]\to X$.
  The group structure is given by concatenation of loops.
\end{definition}
\noindent
The fundamental group defines a homotopy invariant functor:
\[ \pi_1:\rm{Top}_\pt \to \rm{Grp}\]
which will turn out to ``classify'' covering spaces in a precise sense. Observe
that if $x, x\p$ lie in the same (path-)connected component of $X$ then any path
from $x$ to $x\p$ induces an isomorphism $\pi_1(X,x)\rar{\sim} \pi_1(X, x\p)$.
Hence if $X$ is connected we will sometimes be imprecise and denote ``the''
fundamental group of $X$ simply by $\pi_1X$\\\\
In the following for simplicity assume that the base space $X$ is path
connected, locally path connected.
Connectedness can be dropped if one is careful about the statements (The general
statements follow by applying the theorems ``one path component at a time'').
The other assumption is a technical point-set condition that is satisfied
by every ``reasonable space'' i.e.~CW-complexes, Manifolds and so on and is
needed to make the following proposition work:
\begin{proposition}
 Let $Y \rar{p} X$ be a covering and $Z \rar{f} X$ be any map. Let $ z \in Z$ be a point
 and $x=f(z)$. Choose some $y\in Y$ such that $p(y)=x$. Then we can find a unique lift
 in the diagram:
 \[\begin{tikzcd}
	& {Y} \\
	{Z} & {X}
	\arrow["{f}"', from=2-1, to=2-2]
	\arrow["{p}", from=1-2, to=2-2]
	\arrow["{\hat{f}}", from=2-1, to=1-2, dashed]
\end{tikzcd}\]
such that $\hat{f}(z)=y$ if and only if on fundamental groups we have:
\[f_\pt(\pi_1(Z,z)) \subseteq p_\pt(\pi_1(Y,y))\]
\end{proposition}
This is most commonly applied in the case where $Z$ is simply connected,
i.e.~the condition is trivial. In particular we get the following:
\begin{corollary}(Path and Homotopy Lifting)\\
If $Y\to X$ is a covering and $\gamma: I \to X$ is a path, then for every point $y$
in the fiber of $\gamma(0)$ we find a unique lift $\hat{\gamma}:I \to Y$ such
that $\hat{\gamma}(1)=y$. Moreover every homotopy of paths downstairs fixing the
starting and end point lifts to homotopy upstairs also fixing them.
\end{corollary}
Thus if we have a loop $\gamma :S^1 \to X$ the end point of a lift
$\hat{\gamma}: S^1 \to Y$ actually only depends on the homotopy class
$[\gamma]\in \pi_1(X, \gamma(0))$. Moreover since the concatenation of lifts is
the same as the lift of a concatenation (by uniqueness of lifts!) this is
compatible with the group structure on $\pi_1$. A fancy way of putting it is the
following:
\begin{proposition}
  Let $Y \rar{p} X$ be a covering and $x\in X$ a basepoint. Then there is a natural
  action $\pi_1(X,x) \act p^{-1}(x)$ of the fundamental group on the fiber given
  as follows: For $y\in p^{-1}(x)$ and $[\gamma] \in \pi_1(X,x)$ let
  $\hat{\gamma}$ be the lift of $\gamma$ which starts at $y$ and set:
  \[[\gamma]y := \hat{\gamma}(1) \in p^{-1}(x)\]
  i.e.~we map $y$ to the endpoint of the lift.
\end{proposition}
\noindent This is well defined by our considerations so far (Check this!). In
fact we can upgrade this proposition to more structured statement using the
following:
\begin{definition}
  Let $G$ be a group, the category $\set^G$ is the category of sets $S$ together
  with an action $G \act S$. Morphisms of $G$-sets are given my
  \textit{equivariant} maps i.e.~maps $\phi:S \to S\p$ compatible with the $G$
  action in the sense that $\phi(gs)=g \phi(s)$ for all $s\in S, g \in G$.
\end{definition}
\begin{proposition}
  Fix a basepoint $x\in X$, then the assignment:
  \[(Y \rar{p} X) \mapsto (\pi_1(X,x)\act p^{-1}(x))\]
  refines to a functor:
  \[\rm{Cov}_X \to \set^{\pi_1(X,x)}\]
\end{proposition}
\begin{ex}
Show this!
\end{ex}
Now here's the kicker: We claimed earlier that the fundamental group would
``classify'' coverings of $X$ and now we're ready to make this precise. 
\begin{theorem}
  Assume additionally that $X$ is semi locally simply connected. Then the fiber
  functor:
  \[\rm{Cov}_X \to \rm{Set}^{\pi_1(X,x)} \quad (Y \rar{p}X) \mapsto p^{-1}(x)\]
  is an equivalence.
\end{theorem}
This is not an easy theorem, but it tells us basically everything we need to
know about the coverings of $X$ if we analyze the category
$\rm{Set}^{\pi_1(X,x)}$ carefully.
\begin{remark}
  \begin{itemize}
    \item If $\pi_1(X,x)$ a acts on $S$ via the identity (i.e.~fixes every point). Then
      the correspond covering space is the trivial covering:
      \[ S \times X \to X\]
      discussed earlier. In particular the one point set $\{\pt\}$ corresponds
      to the identity covering $X \rar{\id} X$.
      \item Consider the $G$ set given by $G$ itself with action via
        left-multiplication. Note that this action is free and transitive,
        i.e.~the group acts via bijections. The corresponding covering is called
        the \textit{universal covering} of $X$. 
        \item The universal covering admits no nontrivial coverings and thus has
          $\pi_1 =0$ i.e.~is simply connected. Moreover this actually
          characterizes the universal covering i.e.~any covering space $Y \to X$
        with $\pi_1Y =0$ is isomorphic to the universal covering. (This follows
        entirely from the theorem but might be a little tricky)  
        \item This gives us a method of actually computing the fundamental
          group. If we suspect that $\pi_1(X)= G$ for some group all we need to
          do is find a simply connected space $Y$ with a $G$-action that is
          free and properly discontinuous such that the quotient $Y_G$ is
          homotopy equivalent to $X$. Then since the fiber of $Y \to Y_G$
          is naturally a free and transitive $G$-set we get $G\iso \pi_1Y_G\iso \pi_1X$
  \end{itemize}
\end{remark}
\underline{Questions}:
\begin{itemize}
  \item Suppose that $Y$ and $X$ have isomorphic fundamental groups $G$. We see
    immediately that the categories $\rm{Cov}_X$ and $\rm{Cov}_Y$ must be
    equivalent. Does this imply that for a given $G$-set $S$ the associated
    covering spaces in $ X(S) \in \rm{Cov}_X$ and $ Y(S)\in\rm{Cov}_Y$ are homotopy equivalent? 
    \item Note that any $G$-set $S$ uniquely decomposes into the orbits of the
      $G$-action:
      \[ S = \coprod_{[s]\in S_G} Gs\]
      Show that this actually a coproduct in the category $\rm{Set}^G$. For
      $G=\pi_1(X,x)$, what does this decomposition correspond in the category $\rm{Cov}_X$?
      \item Describe the category $\rm{Cov}_{S^1}$ by first describing
        $\rm{Set}^\Z$ and then for every $\Z$-set giving an explicit covering
        space which corresponds to it. (First consider sets where the actions is
        transitive and then use the decomposition)
\end{itemize}
\end{document}