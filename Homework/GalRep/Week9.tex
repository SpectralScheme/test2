\input{/home/flo/Documents/tex/header}
\begin{document}
\begin{exercise}{9.1}
Let $G$ be a profinite group and $\rho: G \to \gl(V)$ be a finite dimensional
$\Q_p$-representation.\\\\ 
\underline{Claim}: The following are equivalent:
\begin{enumerate}
\item The action map $a:G\times V \to V$ is continuous for the discrete topology on $V$
  \item Every $v\in V$ is fixed by an open subgroup of $G$
    \item There exists a basis $v_1, \dots, v_n$ of $V$ such that each $v_i$ is
      fixed by an open subgroup of $G$
      \item $\ker \rho$ is open
\end{enumerate}
\begin{proof}
  
  ``$1.\implies 2.$'' For $v\in V$ the preimage $a^{-1}(v)\subseteq G \times V$
  is by assumption open. Hence the intersection \[a^{-1}(v)\cap G\times \{v\}
  = \left\{ g \in G \mid g(v) =v \right\}_{} \] 
is also open and clearly a subgroup of $G= G\times\{v\}$\\\\
``$2. \implies 3. $'' trivial\\\\
``$3. \implies 4.$'' Let $H_i$ be the open subgroups fixing the $v_i$, then
$H:=\bigcap_iH_i$ is again an open subgroup. Since any $g\in H$ fixes the basis
and hence, by linearity of the action, \textit{all} $v \in V$ we have $H \subseteq
\ker \rho$. Since $H$ is open, $G/H$ is finite and we get that:
\[ \faktor{G}{\ker \rho} = \faktor{\faktor{G}{H}}{\faktor{\ker \rho }{H}}\]
is finite as well. Hence $\ker \rho$ is open as claimed.\\\\
``$4. \implies 1.$'' Since the action map factors as:
\[ G \times V \to \gl(V)\times V \to V\]
it suffices to show that $\rho$ is continuous for the discrete topology on
$\gl(V)$. But for $x\in \gl(V)$ and any $g,G\p\in \rho^{-1}(x)$ we have that:
\[ g^{-1}\cdot g\p \in \ker \rho\]
and hence:
\[ \rho^{-1}(x)=g\cdot \ker \rho\]
so the claim follows since $\ker \rho$ is by assumption open and multiplication
with $g$ is a homeomorphism.
\end{proof}
\end{exercise}
\begin{exercise}{9.2}
Let $K/\Q_p$ be a finite extension,$G= \gal(\bar{\Q}_p/K)$ and $\rho: G \to
\gl(V)$ a $p$-adic Galois representation.\\\\
\underline{Claim}: The following are equivalent:
\begin{enumerate}
\item [(a)] $V$ is smooth
  \item [(b)] $V \otimes_{\Q_p}\bar{\Q}$ admits a basis of $G$-fixed vectors.
\end{enumerate}
\begin{proof}
  ``$(a)\implies (b) $'' if the action on $V$ is continuous with respect to the
  discrete topology then so is the induced action on $V \otimes_{\Q_p}\bar{Q}$.
Hence the claim follows since we have $H^1_{\rm{cont}}(G,
\gl_n(\bar{\Q}_p))=0$.\\
\\
``$(b)\implies (a)$'' Let $w_1, \dots, w_n$ be such a basis and $v_1, \dots v_n$
be a $\Q_p$-basis of $V$. Then $1 \otimes v_i$ forms a basis of $V \otimes_{\Q_p}\bar{\Q_p}$ and hence we can find unique $\alpha_{k,i} \in \bar{\Q}_p$ such that: \[ w_i = \sum_{k=1}^n \alpha_{k,i} \otimes v_k\] Now set $L=K(\{\alpha_{i,k}\}_{i,j})$ then we claim that $\ker \rho = \gal(\bar{\Q}_p / L)$. Indeed for any $\sigma \in G$ using that the $w_i$ are fixed under the action we get: \[  \sum_{k=1}^n \alpha_{k,i} \otimes v_k = w_i = \sigma(w_i) =\sum_{k=1}^n \sigma(\alpha_{k,i}) \otimes \sigma (v_k)\] Now our claim follows from the uniqueness of the expansions. Indeed: If $\sigma \in \ker \rho$ we have $\sigma(v_i)=v_i$ and hence also $\sigma(\alpha_{i,k}) = \alpha_{i,k}$, showing $\ker \rho \subseteq \gal(\bar{\Q}_p/L)$. Conversely if $\sigma$ fixes $L$ then the expansion of $w_i$ has the same coefficients in the two bases $\{v_i\}$ and $\{\sigma(v_i)\}$ and hence already $v_i = \sigma(v_i)$. Now since the $v_i$ generate $V$ $\sigma$ in fact fixes every $v\in V$ and so $\sigma \in \ker \rho$. Hence we have: \[ G/ \ker \rho = \gal(\bar{\Q}_p/K)/\gal(\bar{\Q}_p/L)\iso \gal(L/K)\] which is finite and hence $\rho$ is smooth as claimed. \end{proof} \end{exercise}
\begin{exercise}{9.3}
  \begin{enumerate}
\item [(a)] Let $G$ be a pro-$p$ Group.\\\\
  \underline{Claim}: For any $g\in G$ the group homomorphism $\Z \to G$ sending
  $1 \mapsto g$ extends uniquely to a continuous group homomorphism $\Z_p \to G$.
  \begin{proof}
    Let $N$ be an open normal subgroup of $G$. Then $G/N$ is a $p$-group and
    hence carries a natural $\Z_p$-action (as we showed previously) i.e.~for
    each $g\in G$ we get a unique extension:
\[\begin{tikzcd}
	\Z & G & {G/H} \\
	{\Z_p}
	\arrow[from=1-1, to=1-2]
	\arrow[from=1-2, to=1-3]
	\arrow["{\exists!}"', dashed, from=2-1, to=1-3]
	\arrow[from=1-1, to=2-1]
\end{tikzcd}\]
with kernel given by $p^n \Z_p$ for some $n$. Hence the map is continuous for
the discrete topology on $G/H$. Now since $G$ is profinite these induce a unique
continuous map:
\[ Z_p \to \flim_H G/H \iso G\]
as claimed.
\end{proof}
\item[(b)] Let $K,F$ be finite extensions of $\Q_p$ for $p>2$ and $G= \gal{K^{(\infty)}/K}$.\\\\
  \underline{Claim}: The set of continous group homomorphisms
\[ G \to F^\times\]
is in non-canonical bijection with $U^{(1)}_F$.
\begin{proof}
  Choose a topological generator of $G$ giving an isomorphism $G\iso \Z_p$.
  We've seen already that there are no non-zero continuous morphisms $\Z_p \to
  \Z$ and hence a map:
  \[ \Z_p \to F^\times \iso \Z \times \cl{O}_F^\times\]
  is determined by the map $\Z_p \to \cl{O}_F^\times$. Moreover we know that
  $\cl{O}_F^\times \iso \Z/(p^n-1) \times U^{(1)}_F$ for some $n$ and again there are no
  nontrivial maps $\Z_p\to \Z/(p^n-1)$. Finally in the lecture we computed that
  we have an isomorphism of topological groups:
  \[U^{(1)}_F \iso \Z_p^{\deg(F/\Q_p)}\times G\]
  where $G$ is a finite $p$-group i.e.~$U^{(1)}_F$ is a pro-$p$ group and the
  claim follows from part (1)
\end{proof}
\end{enumerate}
\end{exercise}
\end{document}