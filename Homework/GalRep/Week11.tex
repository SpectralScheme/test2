\input{/home/flo/Documents/tex/header}
\begin{document}
\begin{exercise}{11.1}
  Let $G$ be a profinite group acting continuously on a topological integral
  domain $B$ with fraction field $K$. Moreover let $E = B^G= K^G$. Given $V \in
  \rm{Rep}_F(G)$ write $D_B(V)= ( B \otimes_F V)^G$ and consider the $B$-linear
  $G$-equivariant map:
  \[ \alpha_V: B \otimes_E D_B(V)\to B \otimes_F V, \quad b \otimes m \mapsto bm\]
  \underline{Claim}:
  \begin{enumerate}
  \item[(a)] $\alpha_V$ is injective
    \item[(b)] $D_B(V)$ is an $E$-vector space of dimension at most $\dim_FV$
  \end{enumerate}
  \begin{proof}
    \begin{enumerate}
     \item[(a)]: The proof of proposition 13.2
       shows (not directly but by the same argument) that for any $K$-vector
       space $W$ the natural map:
       \[ K \otimes_E W^G \to W\]
       is injective, so in particular for $W =K \otimes_F V$. Now as an $E$-vector space we have $D_B(V)\iso E^d$ for
       some $d$ and hence we have $B \otimes_E D_B(V)\iso B^d$ as a $B$-module.
       Moreover the map $B^d \rari{} K \otimes_B B^d = K^d$ is clearly
       injective and hence so is the map:
       \[ B \otimes_E D_B(V)\to K \otimes_B B \otimes_E D_B(V)= K \otimes_E
         D_B(V) \]
       by the same argument we see that the map $B \otimes_F V \to K \otimes_F
       V$ is injective and hence so is $D_B(V)\to D_K(V)$ since this is
       preserved by taking fixpoints. Since $K$ is flat over $E$ the map:
       \[  K \otimes_E D_B(V)\to K \otimes_E D_K(V)\]
       is injective as well. Putting all this together we get a diagram:
       \[\begin{tikzcd}
	{K \otimes_E D_K(V) } & {K \otimes_F V} \\
	{B \otimes_E D_B(V)} & {B \otimes_F V}
	\arrow["{\alpha_V}", from=2-1, to=2-2]
	\arrow[hook, from=2-1, to=1-1]
	\arrow[hook, from=2-2, to=1-2]
	\arrow[hook, from=1-1, to=1-2]
\end{tikzcd}\]
and so $\alpha_V$ is injective which we wanted to show.
\item[(b)]: For $d= \dim_ED_B(V)$ and $n = \dim_FV$. By our considerations in
  (a) we know that $\alpha_V$ induces an injective map $R^d \to R^n$ of
  $R$-modules and hence a map $K^d \to K^n$ of $K$-vector spaces, so clearly we
  have $d \leq n$ as claimed.
    \end{enumerate}
  \end{proof}
\end{exercise}
\begin{exercise}{11.2}
  With notation as in 11.1 assume that $B$ is $(F,G)$-regular.\\
  \underline{Claim:}
  \begin{enumerate}
  \item [(i)] $\dim_ED_B(V)= \dim_FV$
    \item [(ii)] $\alpha_V$ is an isomorphism.
      \item [(iii)] There exists a $B$-linear $G$-equivariant iso $B\otimes_F V
        \iso B^d$ for some $d>0$
  \end{enumerate}
  \begin{proof}
    ``(ii) $\implies$ (iii)'' Clear since $B \otimes_E D_B(V)$ is of this form
    and hence $\alpha$ defines the desired isomorphism\\
    ``(iii) $\implies$ (i)'' Also immediate since the equivariant isomorphism gives an
    identification: \[(B \otimes_FV)^G \iso (B^d)^G= E^d\]
    ``(i) $\implies $ (ii)''
  \end{proof}
\end{exercise}
\begin{exercise}{11.3}
Let $K/\Q_p$ be a finite extension and $V$ be a $p$-adic Galois representation
of $G_K$. Let $\chi^{\rm{cyc}}$ be the cyclotomic character and write $\C_p(i):=
\C_p((\chi^{\rm{cyc}})^i)$. Moreover denote:
\[ V\{i\}= (\C_p(i)\otimes_{\Q_p} B)^{G_K}\]
\underline{Claim}: The natural map of $\C_p$-representations:
\[ \xi_V: \bigoplus_{i \in Z} \C_p(-i) \otimes_K V\{i\} \to \C_p \otimes
  _{\Q_p} V\]
is injective.
\begin{proof}
Consider the Hodge-Tate ring $B_{HT}= \C_P[t^{\pm}]= \bigoplus_{i \in \Z}
\C_p(i)$. Observe that, since $G_k$ acts componentwise on $B_{HT}$ and tensor
products commute with direct sums we have that:
\[(B_{HT}\otimes_{\Q_p}V)^{G_K} = (\bigoplus_{i \in
    \Z}\C_p(i)\otimes_{\Q_p}V)^{G_K}= \bigoplus_{i \in
    \Z}(\C_p(i)\otimes_{\Q_p}V)^{G_K}= \bigoplus_i V\{i\}\]
Hence applying Exercise 11.1 to the ring $B=B_{HT}$ for which we've seen that
$B_{HT}^{G_K} = K$ we get that the natural map:
\[(\pt) \quad B_{HT} \otimes_K \bigoplus V\{i\}= \bigoplus_j \C_p(j) \otimes_{K} \bigoplus_i V\{i\} \to \bigoplus_j\C_p(j)\otimes_{\Q_p}V\]
is injective. Now note that for $v \in V\{i\}$ and $\lambda \in \C_p(j)$ the maps sends: 
\[\lambda \otimes v \mapsto \lambda \cdot v \in \C_p(i+j)\otimes_{\Q_p}V\]
Hence if we precompose $(\pt)$ with the antidiagonal:
\[ \bigoplus_{i \in \Z} \C_p(-i) \otimes_K V\{i\} \rari{}\bigoplus_{j \in \Z}
  \bigoplus_{i \in \Z} \C_p(j) \otimes_{K} V\{i\}\]
the resulting map factors through $\C_p(0)\otimes_{\Q_p} V= \C_p
\otimes_{\Q_p}V$ and gives our map $\xi_V$, which is hence injective as a
composition of injective maps. In particular since the $\C_p \otimes_{\Q_p} V$
is finite dimensional so is every $V\{i\}$ and only finitely many of them are non-zero.
\end{proof}
\end{exercise}
\end{document}