\input{/home/flo/Documents/tex/header}
\begin{document}
 \begin{exercise}{7.1}
   Let $K$ be a local field with valuation ring $\cl{O}_K$.
   \underline{Claim}: The following groups are profinite:
   \begin{enumerate}
   \item[(a)] Finite discrete groups.
     \begin{proof}
       Any discrete set is Hausdorff and it is compact iff it is finite.
       Moreover $\{1\}$ is an open compact normal subgroup contained in
       \textit{every} neighborhood of 1
     \end{proof}
     \item[(b)] Closed subgroups $H \subseteq G$ of a profinite group $G$
     \begin{proof}
       Subspaces of Hausdorff spaces are always Hausdorff and moreover closed
       subsets of compact spaces are compact. Now any open neighborhood of $1\in
       H$ is of the form $V \cap H$ for some $V\subseteq G$ open. Since $G$ is
       profinite there exists some open compact normal subgroup $N \subseteq V
       \subseteq G$. Then $N \cap H$ is open hence closed and hence compact, since $H$ is compact, and moreover normal in $H$. 
     \end{proof}
     \item[(c)] The additive group of $\cl{O}_K$
       \begin{proof}
         Follows by combining 7.2 and 7.3 (a) since $\cl{O}_K/\frak{m}^n_K$ is a
         finite discrete group.
     \end{proof}
         \item[(d)] $\gl_n(\cl{O}_K)$
           \begin{proof}
             Observe that 7.3 (a) actually shows that $\cl{O}_K$ is a profinite
             ring. Moreover the functor $ \rm{Ring}\to \rm{Grp} ~R \mapsto \gl_n(R)$ is represented by
             the Hopf algebra $\Z[T_{i,j}][d^{-1}] $ with $ d= \rm{det}(T_{i,j})$ i.e. we
             have:
             \[ \gl_n(R)= \mathrm{Hom}^{}_{\rm{Ring}}(\Z[T_{i,j}][d^{-1}],R)\]
             Thus we prove the following much more general result: \\
             \underline{Claim}: Let $R$ be a profinite ring and $A$ be a ring of
             finite type which is also a co-group object in $\rm{Ring}$. Then the
             group $\rm{Hom}_{\rm{Ring}}(A, R)$ is profinite.\\
             Indeed: If we write $R \iso \flim_i R_i$ for each $R_i$ finite we
             get that:
             \[ \rm{Hom}(A , R)= \rm{Hom}(A, \flim_i R_i)\iso \flim_i
               \rm{Hom}(A, R_i)\]
             Now since $A$ was of finite type each group $\rm{Hom}(A,R_i)$ is
             finite and thus the claim follows.
           \end{proof}
           (Note that I'm not even using 7.2 (c) for the original statement)
   \end{enumerate}
 \end{exercise} 
 \begin{exercise}{7.2}
   Let $I$ be directed set and $(\{G_i\}_{i \in I}, \varphi_{i,j})$ a projective system of finite
   discrete groups.
 \begin{enumerate}
 \item[(a)]\underline{Claim}: We have that $\flim_{i\in I} G_i \subseteq
   \prod_{i\in I}^{}G_i$ is a closed subgroup.
   \begin{proof}
     Let $(g_k)\in \prod_{i\in I}^{}G_i \setminus \flim_{i\in I}G_i$ i.e.~there
     exist $i,j \in I$ such that $\varphi_{i,j}(g_i)\neq g_j$. Then since the
     $G_i$ are discrete and by the definition of the product topology:
     \[ U= \{g_i\}\times \{g_j\}\times \prod_{k \neq i,j} G_k\]
     is an open neighborhood of $(g_k)$ which does not intersect $\flim_i G_i$.
     Thus the latter is closed.
   \end{proof}
   \item[(b)] \underline{Claim}: $\flim_{i\in I}G_i$ is a profinite group.
    \begin{proof}
      Indeed by 7.1 (b) it suffices to show that the product $\prod_i G_i$ is
      profinite. As a product of compact Hausdorff spaces it is compact
      Hausdorff by Tychonoff. Now let $V$ be an open neighborhood of $1 \in
      \prod_i G_i$ then without loss of generality we can assume:
      \[ V = U_{i_1}\times \dots \times U_{i_n}\times \prod_{i \neq \i_k} G_i\]
      for some indices $i_1, \dots, i_n$ and $U_{i_k}\subseteq G_{i_k}$ open. Then set:
      \[ H= \{1\}\times \dots \times \{1\}\times \prod_{i\neq i_k}G_i\]
      Since the $G_i$ are discrete this is an open and hence closed subgroup of
      $\prod_i G_i$. Since the latter is compact $H$ is also compact and
      moreover we see that:
      \[ \prod_i G_i /H \iso G_{i_1}\times \dots \times G_{i_n}\]
      hence $H$ is also normal. This proves the claim.
    \end{proof} 
 \end{enumerate}
 \end{exercise}
 \begin{exercise}{7.3}
   Let $K$ be a local field.\\
   \underline{Claim}: We have the following isomorphisms of topological groups:
   \begin{enumerate}
   \item[(a)] $\cl{O}_K \iso \flim_n \cl{O}_K/\frak{m}^n$
     \begin{proof}
       Let $A$ be any ring and $\varphi_n: A \to \cl{O}_K /\frak{m}^n$ be a
       compatible system of ring maps. This induces a unique map $ \varphi: A
       \to \cl{O}_K$ as follows:\\
       For each $n$ and $a \in A$ choose a lift $\tilde{a}_n \in \cl{O}_K$ of
       $\varphi_n(a)$. Since the $\varphi_n$ are compatible
       i.e. \[\varphi_{n+1}(a) \mod{\frak{m}^n}= \varphi_n(a)\]
       we see that:
       \[ \tilde{a}_n - \tilde{a}_{n+1} \in \frak{m}^n\]
       thus the $\tilde{a}_n$ define a Cauchy Sequence in $\cl{O}_K$. Now we
       define:
       \[ \varphi(a):= \lim_n \tilde{a}_n \in \cl{O}_K\]
       Moreover this does not depend on the choice of lifts since for another
       sequence of lifts $\hat{a}_n$ we see that:
       \[ \tilde{a}_n - \hat{a}_n \in \frak{m}^n \implies \lim_n(\tilde{a}_n -
         \hat{a}_n) = \lim_n \tilde{a}_n - \lim_m \hat{a}_m = 0\]
       By a very similar argument, using that the $\varphi_n$ are ring
       homomorphisms, we see that $\varphi$ is as well. Thus by abstract
       nonsense we have an isomorphism of abstract rings:
       \[ \cl{O}_K \iso \flim \cl{O}_K /\frak{m}^n\]
       To see that this is continuous it suffices to show that the projection
       maps $\cl{O}_K \to \cl{O}_K/\frak{m}^n$ are continuous by the universal
       property of the topology on the right hand side. However this is clear
       since for every $a \in \cl{O}_K/\frak{m}^n$ the preimage is $a +
       \frak{m}^n \subseteq \cl{O}_K$ which is open since $\frak{m}^n$ is open.
       Thus the map is continuous and since both sides are compact Hausdorff it
       is a homeomorphism as claimed.
     \end{proof}
    \item[(b)] $\cl{O}_K^{\times} \iso \flim(\cl{O}_K/\frak{m}^n_K)^{\times}
      \iso \flim_n \cl{O}^\times/U^{(n)}_K$
      \begin{proof}
        The first isomorphism (without accounting for the topology) is immediate from (a) if we observe that:
        \begin{align*}
          \cl{O}_K^\times = \rm{Hom}(\Z[T^\pm], \cl{O}_K)  &\iso  \rm{Hom}(\Z[T^\pm], \flim_i\cl{O}_K/\frak{m}^i)\\
                                                           &\iso \flim_i \rm{Hom}(\Z[T^\pm], \cl{O}_K/\frak{m}^i)\\
                                                           &= \flim_i ( \cl{O}_K/\frak{m}^i)^\times
\end{align*}
Thus we have left to show that $(\cl{O}_K/\frak{m}^i)^\times \iso
\cl{O}_K^{\times}/U^{(n)}_K$, then the fact that the topologies agree again
follows from observing that the projections $\cl{O}_K^{\times}\to
\cl{O}_K^{\times}/U_K^{(n)}$ are continuous. There is a natural map
$ \cl{O}_K^\times/U^{(n)}_K \to (\cl{O}_K/\frak{m}^n_K)^{\times} $ induced from
the composition:
\[ \cl{O}_K^\times \to \cl{O}_K \to \cl{O}_K/\frak{m}^n_K\]
(Running out of time here..)
      \end{proof}
    \item[(c)] $\widehat{\Z}:= \flim_n \Z/n \iso \prod_{p \textnormal{ prime }} \Z_p$
      \begin{proof}
        First observe that the diagram $(\Z/p^n)_{p,n}$ is final in
        $(\Z/n)_n$ by the chinese remainder theorem, hence $\flim_n \Z/n \iso
        \flim_{n,p}\Z/p^n$. Now note that a cone over $(\Z/p^n)_{p,n}$ is the same as a
        series of cones over each prime $(\Z/p^n)_n$ (since there are no maps
        inbetween) and thus the same as as cone over the discrete diagram
        $(\Z_p)_p$. Hence we have $\flim_{n,p}\Z/p^n \iso \lim_p \Z_p = \prod_p
        \Z_p$ as claimed. This is a homeomorphism since everything carries the
        topology induced from the limit.
      \end{proof}
   \end{enumerate}
 \end{exercise}
 \begin{exercise}{7.5}
   Let $K$ be a field with a tower of extensions:
   \[ K=K_0 \subset K_1 \subset \dots \subset \bigcup_n K_n = K_{\infty}\]
   such that each $K_n/K$ is Galois with $\gal(K_n/K)$ cyclic of order $p^n$.\\
   \underline{Claim}: We have a non-canonical topological isomorphism:
   \[ \gal(K_\infty/K)\iso \Z_p\]
   \begin{proof}
     Since $K_\infty/K$ is again Galois we know that we have a canonical iso:
     \[ \gal(K_\infty/K)\iso \flim_L \gal(L/K)\]
     where the limit ranges over all finite Galois extensions $L/K$. Now since
     the $\gal(K_n/K)$ are all cyclic of order $p^n$ and any subgroup of a
     cyclic group is again cyclic we see that any such $L$ mus be equal to $K_n$
     for some $n$. Choosing generators for each $C_{p^n}$ we obtain an isomorphism
     of diagrams:
     \[\begin{tikzcd}
	{\dots} & {C_{p^n}} & {C_{p^{n+1}}} & {\dots} \\
	{\dots} & {\Z/p^n} & {\Z/p^{n+1}} & {\dots}
	\arrow[from=1-3, to=1-2]
	\arrow[from=1-2, to=1-1]
	\arrow[from=2-3, to=2-2]
	\arrow[from=2-2, to=2-1]
	\arrow[from=1-4, to=1-3]
	\arrow[from=2-4, to=2-3]
	\arrow[from=2-3, to=1-3]
	\arrow[from=2-2, to=1-2]
\end{tikzcd}\]
And so:
\[ \gal(K_\infty/K)\iso \flim_n C_{p^n}\iso \flim_n \Z/p^n= \Z_p\]
as desired.
   \end{proof}
 \end{exercise}
\end{document}