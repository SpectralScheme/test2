\input{/home/flo/Documents/tex/header.tex}
\begin{document}
\begin{exercise}{6.1}
Let $K$ be a complete field with respect to a complete valuation $\nu:
K^{\times} \to \Z$ with valuation ring $\cl{O}$ and maximal ideal $\frak{m}$.
Moreover let $\kappa = \cl{O}/\frak{m}$ be perfect of characteristic $p>0$.
Consider the map:
\[ [\blank]: \kappa \to \cl{O}\]
defined as follows: Given $z \in \kappa $ and $n \in \Z$ let $z_n \in \kappa$
with $z_n^{p^n}= z $ and choose a lift $x_n \in \cl{O}$ of $z_n$. Now define
$[z]:= \lim_{n \to \infty} x_n^{p^n}$
\begin{enumerate}
\item [(a)]\underline{Claim}: This map is well defined.
  \begin{proof}
Indeed suppose we have $a = mx + b$ fpr $a,b,x \in \cl{o}$ and $m \in \frak{m}$
with $x \neq 0$ i.e.~we have $a = b\mod \frak{m}$. Then we see that:
\[ a^{p^n} = (mx +b)^{p^n} = b^{p^n}+
  \underbrace{m^{p^n}x^{p^n}}_{ \in~ \frak{m}^{p^n}} + \underbrace{p^nm( \dots
    )}_{\in~\frak{m}^{n+1} \textnormal{ since } \ch{\kappa}=p}\]
by the usual argument that $p$ divides the terms of the binomial expansion. Thus
we see that $ a \equiv b \mod \frak{m}$ implies $a^{p^n}\equiv b^{p^n}\mod
\frak{m}^{n+1}$.
Thus if we have two sequences $y_n, x_n$ lifting the $z_n$ we get that
\[ x^{p^n}_n \equiv y^{p^n}_n  \mod \frak{m}^{n+1}\]
and hence:
\[ \abs{ y_n^{p^n}- x_n^{p^n}} \rar{n \to \infty} 0\]
Since the norm $\abs{\blank}$ associated to $\nu$ is non-archimedian, to see
that $x_n^{p^n}$ is a Cauchy sequence it suffices to prove that:
\[ \abs{x_n^{p^n} - x_{n+1}^{p^{n+1}}} \rar{n\to \infty} 0\]
To see this note that since $x_{n+1} \mod \frak{m}$ is a $p^{n+1}$-th root of
$z$ naturally $x_{n+1}^p \mod \frak{m}$ is a $p^{n}$ root of $z$. Since $\kappa$
is perfect of characteristic $p$ these roots are in fact unique. Thus we see
that:
\[ x_{n+1}^p \equiv x_n \mod \frak{m}\]
and thus by what we already showed:
\[ x_{n+1}^{p^{n+1}} \equiv x_n^{p^n} \mod \frak{m}^{n+1}\]
so the sequence $x_n^{p^n}$ is Cauchy and hence convergent since $K$ is complete.
  \end{proof}
  \item[(b)] \underline{Claim}: The map $[ \blank ]$ is multiplicative.
    \begin{proof}
      For $z, z\p \in \kappa$ roots $z_n, z_n\p$ and lifts $x_n, y_n$ as in the
      construction we have that:
      \[ x_ny_n \equiv z_n z_n\p \mod \rm{m}\]
      and hence we can compute:
      \[ [zz\p]= \lim_{n\to \infty} x_n^{p^n}y_n^{p^n}= \lim_{n\to
          \infty}x_n^{p^n} \lim_{k \to \infty} y_k^{p^k}= [z][z\p]\]
      where the second equality follows either since the multiplication map is
      continuous or by a standard proof from analysis.
    \end{proof}
    \item[(c)] \underline{Claim:} If $\ch(K)=p$ then $[\blank]$ is also additive.
      \begin{proof}
        Let $z, z\p \in \kappa$ and $x_n, y_n\in \cl{o}$ lifts of the $z_n,
        z_n^{\p}$ as in the definition of $[\blank]$. Then since everything is
        in characteristic $p$ we get that:
        \[ (z_n + z_n^{\p})^{p^n}= z_n^{p^n} + (z_n^{\p})^{p^n}\]
        and furthermore:
        \[ (x_n + y_n)^{p^n}= x_n^{p^n} + y_n^{p^n} \equiv z_n^{p^n}+
          (z\p_n)^{p^n}\mod\frak{m^n}\]
        i.e.~ the sequence $x_n^{p^n} + y_n^{p^n}$ converges to $[z+z\p]$. Again
        either since addition is continuous or by the proof for a standard result in analysis we get:
        \[ \lim_n (x_n^{p^n} + y_n^{p^n})= \lim_nx_n^{p^n}+ \lim_m y_m^{p^m}= [z]
          + [z\p]\]
        which shows the claim.
      \end{proof}
      \item[(d)] Let $K$ be a local field. One can show that $[z] \equiv z\mod
        \frak{m}$ and hence $[\blank]$ is a section of the projection:
        \[ \cl{O} \to \kappa\]
        and hence splits the short exact sequence:
        \[ 1 \to U_{1} \to \cl{O}^{\times} \to \kappa^{\times} \to 1\]
        which exhibits $\kappa^\times$ as a torsion subgroup of
        $\cl{O}^{\times}$ i.e.~roots of unity.
\end{enumerate}
 \end{exercise}
\begin{exercise}{6.2}
Let $K/\Q_p$ be a finite extension and $K \subset L \subset \Q_p^{(\infty)}$ an
intermediate field.\\
\underline{Claim}: We have that either $L = K^{(\infty)}$ or $L = K^{(n)}= \Q_p^{(n)}K$
\begin{proof}
First assume that $p\neq 2$ and that $L/K$ is finite. Let $m$ be the largest
integer such that $K$ contains an $p^m$-th primitive root of unity and hence $K^{(m)}=K$. We claim
that for all $n$ we have:
\[ \gal(K^{(n+m)}/K^{(m)}) = \Z/p^n\]
Indeed in the diagram of field extensions:
\[\begin{tikzcd}
	& {K^{(n+m)}} \\
	{K^{(m)}} && {\Q_p^{(n+m)}} \\
	& {\Q_p^{(m)}}
	\arrow[from=2-1, to=1-2, no head]
	\arrow[from=2-3, to=1-2, no head]
	\arrow[from=3-2, to=2-1, no head]
	\arrow[from=3-2, to=2-3, no head]
\end{tikzcd}\]
 We have $K^{(n+m)}= K^{(m)} \Q_p^{(n+m)}$ and $K^{(n)}\cap \Q_p^{(n+m)} =
 \Q_p^{(m)}$ and hence we know from Galois theory that:
 \[ \gal(K^{(n+m)}/K^{(m)}) = \Z/p^n \gal( \Q_p^{(n+m)}= \Q_p^{(m)}= \Z/p^n\]
 as claimed. Now since $L$ is finite there exists some $n$ such that $L$
 contains no $n + m$-th primitive roots of unity and furthermore $L \subset
 K^{(\infty)}$ we get that $K = K^{(m)} \subset L \subset K^{(m+n)}$ thus $L$
 corresponds to a subgroup of $\Z/p^n$ which is necessarily cyclic of some order
 $p^k$ with $k \leq n$. Hence $L= K^{(k+m)}$ since by our computation of Galois group.
 Now if $L/K$ is not finite we know that all finite intermediate extensions $K
 \subset L\p \subset L$ are of the form $K^{(l)}$ for some $l$ since $L
 \subset \Q^{(\infty)}$. Furthermore since $\Q_p^{(\infty)}$ is the smallest extension
 containing these we see that $\Q_p^{(\infty)} \subset L$ and hence they are
 equal.\\
 (p=2 goes here)
\end{proof}
\end{exercise}
\begin{exercise}{6.3}
Let $K$ be a complete field with respect to a discrete non-archimedian absolute
value $\abs{\blank}:K \to \bb{R}_{\geq 0}$. Denote the unique extension to
$K^{sep}$ also as $\abs{\blank}$ and for $\alpha \in K^{sep}$ put:
\[ r(\alpha)= \rm{inf} \left\{ \abs{\sigma(\alpha - \alpha)} \mid \sigma \in
    \mathrm{Hom}^{}_{K}(K(\alpha),\overline{K}) \textnormal{ with }
    \sigma(\alpha) \neq \alpha \right\}\]
\underline{Claim}: For $\alpha, \beta \in K^{sep}$ with $\abs{\beta - \alpha} <
r(\alpha)$ we have that $K(\alpha) \subseteq K(\beta)$
\begin{proof}
First recall that for any $x \in K^{sep}$ the absolute value $\abs{x}$ was defined via the Norm of $x$
which could be computed as:
\[ N_{K(x)/K}= \prod_{\sigma: K \rari{} K^{sep}}^{} \sigma(x)\]
so that $N(x)=N(\sigma(x))$ and hence $\abs{x}= \abs{\sigma(x)}$ for each
embedding $\sigma: K \rari{} K^{sep}$.
Now it suffices to show that any embedding $K(\alpha, \beta) \rari{} K^{sep}$ that
fixes $\beta$ also fixes $\alpha$. Suppose we have an embedding $\sigma$ with
$\sigma(\beta) \neq \beta$ but $\sigma(\alpha)= \alpha$. Then we get that:
\[ \abs{ \alpha - \beta }= \abs{ \sigma(\alpha - \beta)} = \abs{ \sigma(\alpha)-
    \beta}\\
= \abs{\sigma(\alpha)- \alpha + \alpha - \beta} \]
Now by definition of $\beta$ we have that $\abs{\alpha - \beta}<
\abs{\sigma(\alpha)- \alpha}$ and hence:
\[ \abs{ \alpha - \beta}=\abs{\sigma(\alpha)- \alpha + \alpha - \beta} = \rm{max}(\abs{
    \sigma(\alpha)- \alpha}, \abs{\alpha - \beta}) = \abs{\sigma(\alpha)- \alpha}\]
which contradicts the definition of $\beta$. Hence such an embedding $\sigma: K
\rari{} K^{sep}$ cannot exist and we are done.
\end{proof}
\end{exercise}
\end{document}