\input{/home/flo/Documents/tex/header.tex}
\begin{document}
\begin{exercise}{3.1}
Let $a \in \Z$ and $p$ a prime with $p^2 \nmid a$. Note that since
$X^2 - a$ is primitive, the solutions in $\Qp$ are the same as in $\Z_p$ by the
Gauss Lemma, so during the entire exercise we only consider solutions in $\Z_p$.
\begin{enumerate}
\item[(a)] \underline{Claim}: If $p$ divides $a$, then the equation $X^2-a=0$
  has no solutions $\Qp$.
  \begin{proof}
   Suppose we have $b\in \Z_p$ such that $b^2=a$. Since $p$ divides $a$ it
   cannot be a unit, but then neither can $b$ since a square of a unit is a
   unit. Hence $p \mid b$ and thus $p^2 \mid b^2=a$ which contradicts our assumption.
  \end{proof}
    \item[(b)] Suppose $p$ does not divide $a$ and $p\neq 2$
      \begin{enumerate}
      \item[(b1)] \underline{Claim}: If $a$ is a square in $\Z/p$ then $X^2-a$ has two distinct
        roots in $\Qp$
        \begin{proof}
          Suppose we have $b\in \Z/p$ with $b^2=\bar{a} \in \Z/p$, then we get a factorization:
          \[ X^2-\bar{a}= (X-b)(X+b) \in \Z/p[X]\]
          Where $b \neq -b$ since $p \neq 2$. By Hensels Lemma this lifts to a factorization in $\Z_p[X]\subset
          \Qp[X]$ and hence $X^2-a$ has two distinct roots.
        \end{proof}
        \item[(b2)] If $a$ is not a square in $\Z/p$ then $X^2-a$ has no roots
          in $\Qp$
          \begin{proof}
            Again by our observation using the Gauss Lemma, we can assume that
            such a factorization has coefficients in $\Z_p$. Then the mod $p$
            reduction gives a factorization and hence (since $\rm{char}\neq 2$)
            a root of $X^2-\bar{a}$ i.e.~$a$ is square in $\Z/p$.
          \end{proof}
      \item[(c)] Assume that $p=2$ and $p \nmid a$.\\
        \underline{Claim}: The equation $X^2 - a = 0$ has two solutions in
        $\Q_2$ if $ a \equiv 1 ~\rm{mod}~8$ and so solutions otherwise.
        \begin{proof}
          It again suffices to consider solutions in $\Z_p$.
          Suppose we have an element $ b = \sum_{i\geq 0}^{} b_i 2^i \in
          \Z_2$ with $b_i \in \{0,1\}$ such
          that $b^2=a$. Using that we are in characteristic 2 we compute that:
          \[ a= \left( \sum_{i \geq 0}^{}b_i2^i \right)^2= \sum_{i \geq 0
            }^{}b_i^2 2^{i2}= \sum_{i \geq 0}^{}b_i 2^{i2}\]
          So reducing mod $8$ we get that:
          \[\bar{a}= b_0 + 4b_1\in \Z/8\]
          Now it is clear that $b_0 +4b_1 \in \{0,1,2,5\}$. Since by assumption
          we have $\bar{a} \neq 0 \in \Z/2$ it cannot be $0$ or $2$. One checks
      by hand that $5$ is not a square in $\Z/8$, so we get that necessarily
      $\bar{a}=1 \in \Z/8$. This shows one direction.
        \end{proof}
      \end{enumerate}
\end{enumerate}
\end{exercise}
\begin{exercise}{3.2}
Let $p$ be a prime number.
\begin{enumerate}
\item[(a)] \underline{Claim}: Let $M$ be a $\Z_p$-module of finite cardinality.
  Then $M$ is an abelian $p$-group.
  \begin{proof}
    For each $n\in \Z_{>1}$ consider the commutative diagram:
    \begin{equation*}
      \begin{tikzcd}
        \Z \arrow[r] \arrow[rd] & \Z_p \arrow[d] \\
        & \Z/p^n        
      \end{tikzcd}
    \end{equation*}
    Given by the inclusion horizontally and the mod $p$ reduction vertically.
    Since $M$ is finite, so is $\mathrm{Hom}^{}_{\rm{Ab}}(M,M)$ and the action
    $\Z_p\to \mathrm{Hom}^{}_{\rm{Ab}}(M,M)$ has a non-trivial kernel which necessarily contains
    some power $p^n$ and thus the action factors through $\Z/p^n$. Then by our diagram $M$ becomes a $\Z$-module in a compatible manner, i.e.~a finite abelian group which is $p^n$-torsion. By the
    classification of finitely generated abelian groups we geht that there
    exist $k_1,\dots, k_l \leq n$ such that:
    \[M\iso \prod_{i=1}^{l}\Z/p^{k_i}\]
    Thus $\rm{card}(M)=p^{k_1+ \cdots +k_l}$ as claimed.
  \end{proof}
 \item[(b)] \underline{Claim}: Every finite abelian $p$-group is naturally a $\Z_p$-module.
  \begin{proof}
    Again by the classification we can write $M$ as:
    \[M \iso \prod_{i=1}^{l}\Z/p^{k_i} \]
    setting $n=\rm{max}\{k_1, \cdots ,k_l\}$ this is naturally a $\Z/p^n$-module
    since it is $p^n$-torsion and hence a $\Z_p$-module via the reduction $\Z_p
    \to \Z/p^n$ as claimed.
  \end{proof}
\end{enumerate}
\end{exercise}
\begin{exercise}{3.4}
Let $n \in \Z_{\geq 0}$ and $p$ a prime number. Let $\sum_{i=0}^{r}a_i p^i$ with
$a_i < p$ be the $p$-adic expansion of $n$. Put $s(n)= \sum_{i=0}^{r}a_i$.\\
\underline{Claim}: We have that $\nu_p(n!)= \frac{n - s(n)}{p-1}$
\begin{proof}
By definition we have that $\nu_p(n)$ is the first $k$ for which $a_k\neq 0$. We
prove the claim by induction. Clearly we have $\nu_p(1)=0$, so suppose that we
have shown the claim for $n$. Note that, by the properties of a valuation
we get that:
\[ \nu_p((n+1)!)= \nu_p((n)!)+ \nu(n+1)\]
We go case by case:
\begin{enumerate}
\item Suppose $\nu_p(n)\neq 0$, then by the explicit of description of $\nu_p$
  we have that $\nu_p(n+1)=0$ and furthermore $s(n+1) = s(n)+1$. Hence we get
  that:
  \[ \nu_p((n+1)!)= \frac{n - s(n)}{p-1} + 0 = \frac{n - 1 -( s(n+1) -1 )}{p-1}=
    \frac{n+1 - s(n+1)}{p-1}\]
  as desired.
 \item Suppose $\nu_p(n)=0$ then if $\nu_p(n+1)=0$ the same argument as before
   works. If $\nu_p(n+1)=k \neq 0$ we get that:
   \[ s(n+1)= \sum_{i=k}^{r}a_i + 1\]
   Where $a_i$ is still the expansion of $n$ as above and hence:
   \[s(n)= \sum_{i=0}^{r}a_i= s(n+1)-1 + \sum_{i=0}^{k-1}a_i\]
   Plugging this in gives:
   \[ \nu_p(n+1)= \frac{n-1 - s(n)}{p-1} + \nu_p(n+1)= \frac{n-s(n) -
       \sum_{i=1}^{k-1}a_i + k(p-1)}{p-1}\]
   Now the claim follows from the fact that by our choice of $k$ we have:
   \[k(p-1) = \sum_{i=0}^{k-1}(p-1)= \sum_{i=0}^{k-1}a_i\] \qedhere
\end{enumerate}
\end{proof}
\end{exercise}
\end{document}