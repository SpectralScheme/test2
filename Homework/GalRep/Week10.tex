\input{/home/flo/Documents/tex/header}
\begin{document}
\begin{exercise}{10.1}
Let $K/\Q_p$ be a finite extension and $\eta: G_K \to \Q_p^\times$ a continuous
character. Let $\C_p(\eta)$ be the $\C_p$-representation with basis
$e_\eta$ given by $\sigma(e_\eta)= \eta(\sigma)e_\eta$.\\\\
\underline{Claim}: Assume that $\eta(G_{K^{(\infty)}}) =1$ and that $\eta$ has
infinite image, then we have $\C_p(\eta)^{G_K}= \{0\}$
\begin{proof}
Every element of $\C_p(\eta)$ is of the form $\alpha e_\eta$ for some $\alpha
\in \C_p$, hence we have to show that for all $\sigma \in G_K$ we have:
\[ \sigma(\alpha) \eta(\sigma)= \alpha \implies \alpha = 0\]
    First suppose that $\alpha \in \C_p \setminus L$, then since
    $G_{K^{(\infty)}}= \rm{Aut}_{\rm{cont}}(\C_p/L)$ we can find some $\sigma
    \in G_{K^{(\infty)}}$ such that $\sigma(\alpha \neq \alpha)$. Then by a
    assumption $\eta(\sigma)=1 $ and hence $\eta(\sigma)\sigma(\alpha)\neq
    \alpha$.\\
    Now suppose $\alpha \in K$, then necessarily $\sigma(\alpha)=\alpha$ for all
    $\sigma \in G_K$. Since $\eta$ has infinite image we can pick some $\sigma
    \in G_K$ such that $\eta(\sigma)\neq 1$ and we see that:
    \[ \sigma(\alpha)\eta(\sigma)=\alpha \eta(\sigma)=\alpha \iff \alpha =0\]
    So it remains to consider $\alpha \in L \setminus K$. In this case we have
    seen that we get a sequence \[\alpha_n:=\hat{t}_{K^{(n)}}(\alpha) \rar{n\to
        \infty} \alpha\]
    By assupmtion the action of $G_K$ on $K^{(\infty)}$, and hence on $L$,
    factors through $G_{K^{(\infty)}}$. Let $\sigma$ be a topological generator
    of $G_{K^{(\infty)}}$. 
    Since $\alpha \in K$ we can find some $N$ such that
    for all $n\geq N$ $\widehat{t}_{K^{(n)}}(\alpha)\in K^{(n)}\setminus K$
    i.e.~we have $\sigma(\hat{t}_{K^{(n)}}(\alpha)) \neq
    \hat{t}_{K^{(n)}}(\alpha)$. If we can use this and some estimate to show
    that $\abs{\sigma(\alpha)}-\abs{\alpha}$ then we are done since
    $\eta(\sigma)\in \Z_p^\times$ necessarily and hence $\abs{\eta(\sigma)\cdot\sigma(\alpha)}=\abs{\sigma(\alpha)}$.
\end{proof}
\end{exercise}
\begin{exercise}{10.3}
Let $G$ be a profinite group, $R$ a commutative topological ring with a
continuous $G$-action.\\
\underline{Claim}: There is a canonical bijection between isomorphism classes of
$R$-representations of rank $n$ denoted $\pi_0\rm{Rep}^n_G(R)$ and the pointed set $H^1(G, \gl_n(R))$.
\end{exercise}
\begin{proof}
  (We give a slightly modified coordinate-free proof that is equivalent to the one
  using bases)\\
  First observe that any such representation is isomorphic to one on $R^n$ via a
  choice of basis. Denote the componentwise action of $G$ on $R$ by $\rho_0$.
  Then if $\alpha$ is some other action consider the map:
  \[ f_{\alpha}: G \to \gl_n(R), \quad g \mapsto \alpha(g)\circ \rho_0(g)^{-1}\]
  Note that $f_\alpha$ is indeed $R$-linear so this is well-defined.
  Moreover $f_\alpha$ is continuous since $\alpha$ and $\rho_0$ are continuous. Now observe that
  $G$ acts continuously on $\gl_n(R)$ by setting $g\cdot A= \rho_0(g) \cdot A \cdot
  \rho_0(g)^{-1}$Moreover for
  $g,h\in G$ we compute that:
  \begin{align*}
    f_\alpha(gh)=\alpha(gh)\rho_0^{-1}(gh)&=\alpha(g)\alpha(h)\rho_0(h)^{-1}\rho_0(g)^{-1}\\
                           &=\alpha(g)\rho_0(g)^{-1} \rho_0(g) \alpha(h) \rho_0(h)^{-1}\rho_0^{-1}(g)\\
                           &= f(g) \rho_0(g) f(h)\rho_0(g)^{-1} = f(g)(g\cdot f(h))
  \end{align*}
  Hence $f_\alpha$ defines a continuous cocycle. Now if $\alpha,\beta$ are
  isomorphic we can find some $A\in \gl_n(R)$ such that:
  \[ \alpha= A^{-1} \beta A\]
  an hence we see that:
  \[ f_\alpha(g)= A^{-1}\beta(g) A \rho_0(g)^{-1}=
    A^{-1}\beta(g)\rho_0(g)^{-1}\rho_0(g)A\rho(g)^{-1}=A^{-1}f_\beta(g\cdot A )
    \quad (\pt) \]
  hence isomorphic representations give equivalent cocycles and we get a map: 
  \[ \pi_0 \rm{Rep}_G^n(R) \to H^1(G,\gl_n(R)),\quad \alpha \mapsto f_\alpha\]
  where the cohomology is considered with respect to the $G$-action described
  above. Note that by definition $\rho_0$ is mapped to the trivial cocycle.
  However we could have constructed the map with respect to \textit{any}
  representation, hence our bijection is only as canonical as $\rho_0$ is!\\
  Now given a cocycle $c:G \to \gl_n(R)$ define a $G$-action via:
  \[  \phi_c:G\times R^n \to R^n ,\quad (g,x) \mapsto (c(g)\rho_0(g))(x)  \]
  note that this is semi-linear since $c(g)$ is linear and $\rho_0(g)$ is
  semi-linear. Moreover it is continuous since $f$ and $\rho_0$ are continuous
  as well. Doing the computation $(\pt)$ backwards we see that
  equivalent cocylces give isomorphic representations and hence we get a map:
  \[ \phi:H^{1}(G, \gl_n(R))\to \pi_0\rm{Rep}_G^{n}(R)\]
  which is immediately seen to be inverse to $f$, and hence we get a bijection
  as desired.
\end{proof}
The cool thing about this proof (in my opinion), is that it works much more
generally! Take any object $X$ in some category $\cl{C}$ and for some group $G$
consider the category $\rm{Rep}_G(X)$ of $G$-representations on $X$ with morphisms given by
$G$-equivariant morphisms in $\cl{C}$. Then the above proof shows that, after
choosing any representation as a ``basepoint'' we get a bijection:
\[ \pi_0\rm{Rep}_G(X)\iso H^1(G, \rm{Aut}_{\cl{C}}(X))\]
This is not strictly a generalization since we need to take the topologies into
account but you can just work with (topologically) enriched categories and I
think everything goes through. \\\\
(I spent all my time thinking about sheaves, group actions and semi-linear
representations so this is all I managed to prove, sorry about that)
\end{document}