(TeX-add-style-hook
 "header"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "a4paper" "text={16.5cm,25.2cm}" "centering")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "amsmath"
    "amsthm"
    "amssymb"
    "amsfonts"
    "graphicx"
    "mathtools"
    "tikz-cd"
    "hyperref"
    "mathrsfs"
    "calligra"
    "faktor"
    "enumerate"
    "eucal"
    "xfrac"
    "geometry")
   (TeX-add-symbols
    '("tow" 4)
    '("nseq" 4)
    '("zseq" 5)
    '("pullback" 8)
    '("po" 8)
    '("sq" 8)
    '("adj" 4)
    "perf"
    "B"
    "spec"
    "der"
    "im"
    "id"
    "aut"
    "set"
    "cov"
    "gal"
    "fin"
    "ns"
    "ob"
    "nat"
    "pr"
    "et"
    "alg"
    "el"
    "sch"
    "aff"
    "un"
    "ch"
    "abgrp"
    "rnk"
    "gl"
    "Sp"
    "Ql"
    "Qp"
    "co"
    "pic"
    "ext"
    "Rhom"
    "M"
    "thh"
    "map"
    "hh"
    "AssAlg"
    "ring"
    "tr"
    "prl"
    "stp"
    "stab"
    "ev"
    "cmon"
    "cat"
    "fun"
    "exi"
    "F"
    "act"
    "Q"
    "C"
    "Z"
    "rar"
    "lar"
    "rari"
    "ens"
    "cl"
    "empt"
    "scr"
    "p"
    "G"
    "ul"
    "frk"
    "sub"
    "eps"
    "blank"
    "iso"
    "A"
    "E"
    "pt"
    "op"
    "ab"
    "bb"
    "fcolim"
    "flim")
   (LaTeX-add-amsthm-newtheorems
    "theorem"
    "proposition"
    "lemma"
    "corollary"
    "remark"
    "definition"
    "notation"
    "ex")
   (LaTeX-add-mathtools-DeclarePairedDelimiters
    '("abs" "")))
 :latex)

