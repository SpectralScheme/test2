\section{Non-abelian derived functors}
Previously $F: \cl{A} \to \cl{B}$ additive, then we had two steps:
\begin{enumerate}
\item $K(F): K{\cl{A}}\to K(\cl{B})$
\item $LF = \rm{Ran}(K(F))$
\end{enumerate}
Assume that $K(\cl{A})$ has enough $K$-projectives, then we have:
\begin{enumerate}
\item $K(\cl{A})^{proj}\rari{i} K(\cl{A})\rar{p} D(\cl{A})$
  is an equivalence and $i$ is left adjoint to the projection $p$
  \item for any $F: K(\cl{A})\to \cl{D}$ the left derived functor $LF:
    D(\cl{A})\to \cl{D}$ under this identification is given by the restriction
    of $F$ along $i$.
    \item In particular $LF: D(\cl{A})\to D(\cl{B})$ preserves connective
      objects i.e.:
      \[LF(D(\cl{A}))_{\geq 0}) \subseteq D(\cl{B})_{\geq 0}\]
\end{enumerate}
\underline{Questions}:
\begin{itemize}
\item How can we universally characterize $p\circ K(F):K(\cl{A})_{\geq 0}\to D(\cl{B})$?
  \item What if $F$ is not additive? 
\end{itemize}
\begin{ex}{}
  Given a pointed functor $F: \cl{A}\to \cl{B}$ between abelian categories,
  if there exists a functor $K(\cl{A})_{\geq 0}\to D(\cl{B})$ given on chain
  complexes representing objects by applying $F$ levelwise, then $F$ must
  already be additive.
\end{ex}
\subsection{Yoneda Lemma}
We fix three Grothendieck universes:
\[ \{\text{small sets}\} \subset \{\text{large sets}\} \subset \{\text{very large sets}\}\]
Then we have a notion of categories and $\infty$-categories in all of these:
A small $\infty$-categories is a small simplicial set i.e.~small set of objects,
maps etc are small. Similarly for large and very large.
Now $\cl{S}$ is the large $\infty$-category of small spaces and denote
$\widehat{\cl{S}}$ the very large $\infty$-category of large spaces
($\cl{S}\subset \widehat{\cl{S}}$)
\begin{itemize}
\item Now for a large $\infty$-category $\rm{Map}_{\cl{C}}(a,b)\in \widehat{\cl{S}}$
  \item $\rm{Cat}^{small}_{\infty}$ the large $\infty$-category of small $\infty$-categories
    \item $\rm{Cat}_{\infty}$ the very large $\infty$-category of
      large $\infty$-categories
    \item $\cl{C}\in \rm{Cat}_{\infty} \implies \cl{C}[W^{-1}]\in \rm{Cat}_{\infty}$
      \item $\rm{Map}_{D{\cl{A}}}(X, Y)\simeq \fcolim_{\widehat{X}\to X}
        \rm{Map}(\widehat{X}, Y) \in \widehat{\cl{S}}$
\end{itemize}
\begin{definition}
We say that a large $\infty$-category $\cl{C}$ is \textit{locally small} if for
any pari $a,b \in \cl{C}$ the space:
\[ \rm{Map}_{\cl{C}}(a,b)\in \widehat{\cl{S}}\]
is equivalent to an object in $\cl{S}$ i.e.~it is essentially small. In this
case the functor:
\[ \rm{Map}_{\cl{C}}(\blank, \blank): \cl{C}\op\times \cl{C}\to \widehat{\cl{S}}\]
factors through $\cl{S}\subset \widehat{\cl{S}}$
\end{definition}
\underline{Construction}: For any large $\infty$-category $\cl{C}$ we have a
functor:
\[ j : \cl{C}\to \rm{Fun}(\cl{C}\op, \widehat{\cl{S}})= \widehat{P}(\cl{C})\]
\[ c \mapsto \underline{c} = \rm{Map}_{\cl{C}}(\blank, c)\]
called the \textit{Yoneda embedding}. Moreover if $\cl{C}$ is locally small then
this factors as:
\[ j :\cl{C}\to \rm{Fun}(\cl{C}\op, \cl{S})= P(\cl{S})\]
\begin{theorem}{Yoneda Lemma}
\begin{enumerate}
\item The functor $j$ is fully faithful
  \item For any $F : \cl{C}\op \to \widehat{\cl{S}}$ and any $x \in \cl{C}$
    there is a natural equivalence:
    \[ \rm{Map}_{\rm{Fun}(\cl{C}\op, \cl{S})}(\underbar{x}, F)\simeq F(x)\]
    \item Every object $F\in \rm{Fun}(\cl{C}\op, \widehat{\cl{S}})$ is a (large)
      colimit of objects of the form $\underbar{x}$ for $x\in \cl{C}$
\end{enumerate}
\end{theorem}
\begin{proof}
  ad (3): For a given $F:\cl{C}\op \to \widehat{\cl{S}}$ consider the pullback
  of $\infty$-categpries:
  \[
\begin{tikzcd}
  \cl{C}_F \arrow[d, ""] \arrow[r, ""] & \rm{Fun}(\cl{C}\op, \widehat{\cl{S}})/F \arrow[d, ""]\\
  \cl{C} \arrow[r, "j"] & \rm{Fun}(\cl{C}\op, \widehat{\cl{S}})
\end{tikzcd}
  \]
  Then $\cl{C}/F$ is large. Now we claim that:
  \[ F\simeq \colim_{x\in \cl{C}/F}\underline{x}\]
\end{proof}
Now if $\cl{C}$ is small, then every functor $F:\cl{C}\to \cl{S}$ is a small
colimit of representables\\
Now let $F: \cl{C}\to \cl{D}$ be a functor where $\cl{C}$ is small and $\cl{D}$
possibly large.
\begin{proposition}
  In this setup assume that $\cl{D}$ admits all small colimits, then:
  \begin{enumerate}
  \item There is an essentially unique colimit preserving functor such that we
    have a commutative diagram of $\infty$-categories:
\[\begin{tikzcd}
	{\cl{C}} & {\cl{D}} \\
	{P(\cl{D})}
	\arrow["{j}"', from=1-1, to=2-1, hook]
	\arrow["{F}", from=1-1, to=1-2]
	\arrow[from=2-1, to=1-2]
\end{tikzcd}\]
   (This also defines a left Kan extension) 
   \item If $\cl{D}$ is locally small then this is left adjoint to the
     restricted Yoneda embedding:
     \[ D \to \rm{Fun}(\cl{D}\op ,\cl{S}) \rar{F^\pt} \rm{Fun}(\cl{C}\op ,
       \cl{S})) = P(\cl{C})\]
  \end{enumerate}
\end{proposition}
\begin{corollary}
We have that $\rm{Fun}(P(\cl{C}), \cl{D})\simeq \rm{Fun}(\cl{C}, \cl{D})$ i.e.~
$P(\cl{C})$ is the universal $\infty$-category obtained from $\cl{C}$ by freely
adjoining small colimits.
\end{corollary}
\underline{Construction}: Let $K$ be any class of small colimit shapes e.g. all
colimits, finite colimits, filtered, geometric realizations and so on. We form
$P^K(\cl{C})\subset P(\cl{C})$ as the smallest full subcategory which contains
representables and which is closed under $K$-indexed colimits.
\begin{proposition}
We have for any large $\infty$-category $\cl{D}$ which admits $K$-index colimits
that restriction along $j : \cl{C}\to P^K(\cl{C})$ is an equivalence:
\[ \rm{Fun}^{K}(P^K(\cl{C}), \cl{D}) \rar{j^{\pt}}\rm{Fun}(\cl{C}, \cl{D})\]
where the left term is the category of $K$-indexed colimit preserving functors
and the inverse is given by left Kan extension.
\end{proposition}
\begin{example}
  For $\cl{C}$ any $\infty$-category we have:
  \[\rm{Ind}(\cl{C}) = P^{\rm{filtered}}(\cl{C})\]
  In particular if $\cl{C}$ is a 1-category then so is $\rm{Ind}(\cl{C})$
\end{example}
\begin{proposition}
Assume that $\cl{A}$ has enough compact projective objects. Then we have an
equivalence:
\[ D(\cl{A})_{\geq 0} \simeq K(\cl{A^{\rm{proj}}})_{\geq 0} \simeq
  P^{\Delta^{\op}\rm{filt}}(\cl{A}^{\rm{cp}})\simeq
  \rm{Fun}^{\Pi}((\cl{A}^{\rm{cp}})\op, \cl{S})\]
where the rightmost term are finite product preserving functors.
\end{proposition}
\begin{corollary}
  A functor:
  \[ D(\cl{A})_{\geq 0}\to \cl{E}\]
  that preserves geometric realizations and filtered colimits is uniquely
  determined by its restriction to the subcategory of compact projectives $\cl{A^{\rm{cp}}}$
\end{corollary}
\begin{definition}
Let $\cl{C}$ be an $\infty$-category, then $x\in \cl{C}$ is called
\textit{compact} if $\rm{Map}_{\cl{C}}(x, \blank)$ commutes with geometric realizatons. Moreover if
$\cl{C}$ is a 1-category it is called compact if $\rm{Hom}_{\cl{C}}(x, \blank)$
commutes with split coequalizers.
\end{definition}
\begin{definition}
Let $\cl{C}$ be an ordinary category which admits small colimits and is
generated under small colimits by $\cl{C}^{\rm{cp}}$. Then the animation
$\rm{Ani}(\cl{C})$ is defined as: \[P^{\Delta, \rm{filt}}(\cl{C}^{\rm{op}})
\subseteq \rm{Fun}^{\prod}((\cl{C}^{\rm{cp}}, \cl{S})\]
\end{definition}
\begin{example}
$\rm{Ani}(\rm{Set})\simeq \cl{S}$
\end{example}
\section{HKR and the Cotangent Complex}
Recall: Start with a 1-category $\cl{C}$ which is generated by compact
projective objects ( where $K \in \cl{C}$ was called projective if $\rm{Hom}_{\cl{C}}(K,
\blank)$ preserves filtered colimits and reflexive coequalizers. That $\cl{C}$
is generated by these means that $\rm{Hom}_{\cl{C}}(K, \blank)$ detects isomorphisms )
Then we get an $\infty$-category:
\[ \rm{Ani}(\cl{C})= \rm{Fun}^{\Pi}( (\cl{C}^{\rm{cp}})\op, \cl{S})\]
This is the $\infty$-category freely generated from $\cl{C}\op$ under filtered
colimits and geometric realizations. More precisely:
\begin{itemize}
\item $\cl{C}\op \subseteq \rm{Ani}(\cl{C})$ full subcategory
  \item $\rm{Ind}(\cl{C}^{\rm{cp}}) \subseteq \rm{Ani}(\cl{C})$
    \item An arbitrary object $X \in \cl{C}$ can be represented as a geometric
      realization of a simplicial diagram in $\rm{Ind}(\cl{C}^{\rm{cp}})$
    \item For $X\in \cl{C}^{\rm{cp}}$ we have that:
      \[\rm{Map}_{\rm{Ani}(\cl{C})}( X, \colim_{j \in \Delta\op}Y_j) = \colim_{j
        \in \Delta \op} \rm{Map}_{\rm{Ani}(\cl{C})}(X, Y_j)\]
\end{itemize}
 $D(\cl{A})_{\geq 0} \simeq \rm{Ani}(\cl{A})$\\
  For a not necessarily additive functor $F:\cl{A}\to \cl{B}$ we can compose
  with the Yoneda embedding $\cl{B}\to \rm{Ani}(\cl{B})$. This uniquely extends
  to a functor whihc preserves fileterd colimits and geometric realizations:
  \[ \rm{Ani}(\cl{A})\to \rm{Ani}(\cl{B})\]
\begin{example}
  Consider the functor:
  \[ \Lambda^n_R : \M_R \to M_R\]
  gives a derived functor:
  \[ L\Lambda^n_R : D(R)_{\geq 0} \to D(R)_{\geq 0}\]
  which is computed by representing objects by simplicial diagrams of projective
  modules and then applying the functor levelwise
\end{example}
\begin{ex}
Show that: $L\Lambda^2_{\Z}(\Z/n,y) \simeq \Z/n [1]$ 
\end{ex}
We have that:
\[\rm{ani}(\rm{cRing}) = \rm{Fun}^{\Pi}((\rm{cRin}^{\rm{cp}})\op, \cl{S})\simeq
  \rm{Fun}^{\Pi}((\rm{Poly}^{\rm{fg}})\op, \cl{S})\]
\[ \rm{Ind}(\rm{cRing}^{\rm{cp}})= \rm{Poly}\]
So an object is represented by a simplicial diagram of polynomial rings and
we have:
\[\rm{Map}_{\rm{Ani}(\rm{cRing})}(\Z[x], \colim_{\Delta\op}Y_i) =
  \rm{colim}_{\Delta \op} Y_i\]
\begin{example}
  The functor
  \[ HH(\blank/\Z): \rm{Poly} \to D(\Z)_{\geq 0}\]
  commutes with filtered colimits and so it extends to a functor :
  \[LHH: \rm{Ani}(\rm{cRing})\to D(\Z)_{\geq 0}\]
  which on $ R \in\rm{cRing}$ agrees with $HH(R/\Z)$ (In fact it agrees for all
  animated rings by considering the associated DGA).
\end{example}
\noindent
\underline{Construction}: For $C\in D(\Z)$ we have a map $\tau_{\geq n}C \to C$
which is an iso on $H_i$ for $\geq n$ and the object $C$ has $H_i(\tau_{\geq n}C) = 0$ for all $i <
n$. In fact this defines a functor:
\[ \tau_{\geq n}: D(\Z)\to D(\Z)\]
\begin{ex}
  Construct the functor $\tau_{\geq n}$ using an adjunction.
\end{ex}
We also have maps $\tau_{\geq n+1}C \to \tau_{\geq n}C$ and one sees that the
cofiber of this map is equivalent to the Eilenberg MacLane space $H_n(C)[n]$. \\
For $R \in \rm{Poly}$ by what we've already shown we see that:
\[\begin{tikzcd}
	& {\tau_{\geq n+1} HH(R/Z)} \\
	& {\tau_{\geq n}HH(R/\Z)} & {\Omega^n_{R/\Z}[n]} \\
	& {\vdots} \\
	{} & {\tau_{\geq 0}HH(R/\Z)}
	\arrow[from=3-2, to=4-2]
	\arrow[from=2-2, to=3-2]
	\arrow[from=1-2, to=2-2]
	\arrow["{\mathrm{cofib}}", from=2-2, to=2-3]
\end{tikzcd}\]
\begin{definition}
  We define
 \[ F^n_{HKR}HH(\blank, \Z): \rm{Ani}(\rm{cRing})\to D(\Z)_{\geq 0}\]
 as the nonabelian derived functor of $\tau_{\geq n}HH(\blank/\Z)$ 
\end{definition}
Then we get a cofiber sequence:
\[ F^{n+1}_{\rm{HKR}}(HH(R/\Z))\to F^n_{\rm{HKR}}HH(R/\Q)\to L\Omega^n_{R/\Z}[n]\]
\begin{lemma}
We have that $F^n_{\rm{HKR}}HH(R/\Z) \in D(\Z)_{\geq n}$
\end{lemma}
\begin{proof}
This follows immediately since $D(\Z)_{\geq n}$ is closed under colimits.
\end{proof}
\begin{lemma}
  If $F: \rm{cRing} \to \ab$ commutes with reflexive coequalizers then:
  \[ H_0(LF(R))= F(R)\]
\end{lemma}
\begin{proof}
Resolve $R$ by a simplicial diagram of polynomial rings $R_{\bullet}\to R$, then
in fact:
\[R = \rm{coeq}(R_0 \substack{\rightarrow\\[-1em] \leftarrow \\[-1em] \rightarrow} R_0)\]
Since $LF(R)$ is the complex associated to $F(R_{\bullet})$ the homology
$H_0(LF(R))$ is precisely this coequalizer.
\end{proof}
\begin{ex}
  Show that the following functors commute with reflexive coequalizers:
  \begin{enumerate}
  \item $\rm{cRing} \to \set,~R\mapsto R^n$
    \item $\rm{cRing} \to \rm{Ab},~ R\mapsto \Z[R^n]$
      \item $\Omega^n_{\blank/\Z}: \rm{cRing}\to \rm{Ab}$
  \end{enumerate}
\end{ex}
\begin{theorem}{HKR Version 2}
If $\L\Omega^n_{R/\Z}$ has homology concentrated in degree 0 for each $n$, then
HKR holds for $R$.
\end{theorem}
\begin{proof}
  The long exact sequence on homology associated to the cofiber sequence:
\[F^{n+1}_{\rm{HKR}}\to F^n_{\rm{HKR}}\to \L\Omega^n_{R/\Z}[n]\]
shows that:
\[ H_n(F^n_{\rm{HKR}})\rar{\sim}\Omega^n_{R/\Z}\]
is an isomorphism and furthermore in higher degrees:
\[ H_i(F^{n+1}_{\rm{HKR}})\rar{\sim}H_i(F^{n}_{\rm{HKR}})\]
is an isomorphism for $i >n$. Hence we get equivalences:
\[ \Omega^n_{R/Z}\rar{\sim} H_n(F^{n}_{\rm{HKR}})\rar{\sim} H_nF^0_{\rm{HKR}}= HH_n(R/\Z)\]
\end{proof}
\begin{proposition}
  We have that $L\Omega^n_{R/\Z}$ agrees with the value on $R$ of:
  \begin{align*}
    \rm{Ani}(\rm{cRing}_{/R})\to D(R)_{\geq 0} \to D(\Z)_{\geq 0}\\
    A \in \rm{Poly}_{/R} \mapsto R \otimes_A \Omega^n_{A/\Z}
  \end{align*}
\end{proposition}
Using this we see that:
\[ R \otimes_A \Omega^n_{A/\Z} \iso \Lambda^n_R (R\otimes_A \Omega^1_{A/\Z})\]
  \begin{ex}
Show that $ A\mapsto R \otimes_A \Omega^1_{A/k}$ takes compact projctive objects
in $k\rm{Alg}_{/R}$ to compact projective objects in $\rm{Mod}_R$
  \end{ex}
  Thus we have that:
  \[ L\Omega^n_{R/\Z}= L \Lambda^n_R(L\Omega^1_{R/\Z})\]
  \begin{proposition}
If $L \Omega^1_{R/\Z}$ has homology concentrated in degree 0 and
$\Omega^1_{R/\Z}$ is a flat $R$-moudle that $L\Omega^n_{R/\Z}$ is also
concentrated in degree 0
  \end{proposition}
  \begin{proof}
    If $\Omega^1_{R/\Z}$ is finitely generated + projective then:
    \[ L \Lambda^n_R(\Omega^1_{R/\Z})\simeq \Lambda^n_R\Omega^1_{R/\Z}[0]\]
    In general use Lazard's theorem, i.e.~every flat $R$-module is a filtered
    colimit of finitely generated projective ones.
  \end{proof}
  \begin{theorem}[HKR Final Version]
If $L\Omega^1_{R/\Z}$ has homology concentrated in degree 0 and
$\Omega^1_{R/\Z}$ is a flat $R$-module then:
\[ HH_{n}(R/\Z) \simeq \Omega^n_{R/\Z}\]
  \end{theorem}
  \begin{remark}
    One can replace $\Z$ with some commutative base ring $k$ and everything
    works the same.
  \end{remark}
 \section{The Cotangent Complex and Obstruction Theory}
 We defined $L\Omega^1_{\blank/k}$ by taking the non-abelian derived functor:
 \[ \Omega^1_{\blank/K}: k\rm{Alg} \to \rm{Ab}\]
 and hence it defines a functor:
 \[ \rm{Ani}(k\rm{Alg})\to D(\Z)_{\geq 0}\]
 \begin{lemma}
   The following agree:
   \begin{enumerate}
   \item $L\Omega_{\blank/k}: \rm{Ani}(k\rm{Alg}) \to D(\Z)_{\geq 0}$ evaluated
     on $R$
     \item $L(A \mapsto R\otimes_A \Omega^1_{A/k}): \rm{Ani}(k\rm{Alg}_{/R} \to
       D(R))_{\geq 0}$ evaluated on $R \rar{\id} R$
   \end{enumerate}
 \end{lemma}
 \begin{proof}
 Write $R$ as $\colim_{i \in\Delta\op} A_{i}$ for $A_{i}$ levelwise a
polynomial ring. Then the second expression is given by:
 \begin{align*}
   \colim_{i \in\Delta\op}R \otimes_{A_i} \Omega^1_{A_i/k} &\simeq \colim_{i \in \Delta \op} \left(\colim_{j \in \Delta\op} A_j\right) \otimes^L_{A_i} \Delta^1_{A_i/k}\\
                                                           &\simeq \colim_{i \in \Delta\op}\left( \colim_{j \in \Delta\op_{i/}} A_j \right) \otimes^L_{A_i} \Omega^1_{A_i/k}\\
                                                           &\simeq \colim _{(i \to j) \in (\Delta\op)^{\Delta^1}} A_j \otimes_{A_i} \Omega^1_{A_i/k}\\
   &\simeq \colim_{ i \in \Delta\op} \Omega^1_{A_i/k} = L\Omega^{1}_{R/k}
 \end{align*}
 \end{proof}
 The functor:
 \[ \rm{Ani}(k\rm{Alg}_/R)\to D(R)_{\geq 0}\]
Nowe also preserves finite coproducts and ,since by definition it already
preserves filtered colimits and geometric realizations, hence it preserves all colimits.
\begin{example}
For $R = k[x_1, \dots, x_n]/(f_1, \dots, f_m)$ for a regular sequence $f_i$
then:
\[\begin{tikzcd}
	{k[f_1, \dots, f_m]} & {k[x_1, \dots, x_n]} \\
	{k} & {R}
	\arrow[from=1-1, to=1-2]
	\arrow[from=1-2, to=2-2]
	\arrow[from=1-1, to=2-1]
	\arrow[from=2-1, to=2-2]
	\arrow["\lrcorner"{very near start, rotate=180}, from=2-2, to=1-1, shift right=1, phantom]
\end{tikzcd}\]
is a pushout in $\rm{Ani}(k\rm{Alg}_{/R})$ hence the functor $L(R
\otimes_{\blank}\Omega^1_{\blank/k}$ takes this diagram to the pushout in
$D(R)_{\geq 0}$
\[\begin{tikzcd}
	{R\{df_1, \dots df_n\}} & {R\{dx_1, \dots, dx_n\}} \\
	{0} & {L\Omega^1_{R/k}}
	\arrow[from=1-1, to=2-1]
	\arrow[from=2-1, to=2-2]
	\arrow[from=1-2, to=2-2]
	\arrow["{(\partial_if_j)_{i,j}}"', from=1-1, to=1-2]
\end{tikzcd}\]
Where the horizontal map is the Jacobi Matrix. Hence we have that
$L\Omega^1_{R/K}$ has $H_0$ given by the cokernel of the Jacobi and $H_1$ given
by the kernel.
\end{example}
\begin{definition}
For a commutative $k$-algebra $R$ we call $L\Omega^1_{R/k}$ the
\textit{Cotangent Complex} of $R$ and denote it $L_{R/k}$
\end{definition}
Given a commutative $k$-algebra $S$ and an $S$-module $M$  we have the \textit{split square-zero
  extension} denoted $S \oplus M$. Then one can consider the lifting problem:
\[\begin{tikzcd}
	& {S \oplus M} \\
	{R} & {S}
	\arrow["\phi",from=2-1, to=2-2]
	\arrow[from=1-2, to=2-2]
	\arrow[from=2-1, to=1-2, dashed]
\end{tikzcd}\]
Such a lift corresponds to a $\phi$-linear derivation $R \to M$ i.e.~an
$R$-module map:
\[ \Omega^1_{R/k} \to \phi_{\pt}M\]
where $\phi_{\pt}M$ denotes the restriction of scalars.
\begin{proposition}
\begin{enumerate}
\item There is a functor:
  \begin{align*}
    D(S)_{\geq 0} &\to \rm{Ani}(k\rm{Alg})_{/S}\\
    P &\mapsto S \oplus P \qquad \textnormal{ for } P \textnormal{ projective }
  \end{align*}
\item We have an equivalence of mapping spaces:
  \[ \rm{Map}_{D(R)_{\geq 0}}( L_{R/k}, \phi_{\pt}M) \simeq \rm{fib}_{\phi}\left(
      \rm{Map}_{\rm{Ani}(k\rm{Alg})}(R, S\oplus M) \to
      \rm{Map}_{\rm{Ani}(k\rm{Alg})}(R, S) \right)\]
\end{enumerate}
\end{proposition}
A surjective map of commutative $k$-algebras $\widetilde{R} \to R$ with kernel
$I$ satisfying $I^2=0$ is called a (not necessarily split) \textit{square zero
  extension}. ow $L_{R/\widetilde{R}}$ has $H_0=0$ and $H_1 =
R\otimes_{\widetilde{R}}I =I$. We have a tautological map of $R$-modules:
\[ L_{R/\widetilde{R}}\to I[1]\]
inducing an isomorphism on $H_1$ corresponding to a map of animated
$\widetilde{R}$-algebras:
 \[R \rar{\delta} R \oplus I[1]\]
 \begin{proposition}
   The diagram:
   \[\begin{tikzcd}
	{\widetilde{R}} & {R} \\
	{R} & {R \oplus I[1]}
	\arrow[from=1-1, to=2-1]
	\arrow[from=1-1, to=1-2]
	\arrow["{s}", from=1-2, to=2-2]
	\arrow["{\delta}"', from=2-1, to=2-2]
	\arrow["\lrcorner"{very near start, rotate=0}, from=1-1, to=2-2, phantom]
\end{tikzcd}\]
is a pullback of animated $\widetilde{R}$-algebras.
 \end{proposition}
In summary: Square zero extensions $ I \to \widetilde{R}\to R$ of $k$-algebras correspond to
maps:
\[ L_{R/k}\to I[1]\]
Moreover maps $S \to \widetilde{R}$ lifting a given map $S \to R$ correspond to
lifts:
\[\begin{tikzcd}
	& {\widetilde{R}} & {R} \\
	{S} & {R} & {R \oplus I[1]}
	\arrow[from=1-2, to=1-3]
	\arrow["{s}", from=1-3, to=2-3]
	\arrow[from=1-2, to=2-2]
	\arrow[from=2-1, to=2-2]
	\arrow[from=2-1, to=1-2]
	\arrow[from=2-1, to=1-3, dashed]
	\arrow["{\delta}", from=2-2, to=2-3]
\end{tikzcd}\]
or equivalently a lift in the diagram:
\[\begin{tikzcd}
	&& {R} \\
	{S} & {R \oplus I[1]} \\
	&& {R}
	\arrow[from=2-1, to=2-2]
	\arrow[from=2-1, to=3-3]
	\arrow["{\id}", from=1-3, to=3-3]
	\arrow[""{name=0, inner sep=0}, from=2-1, to=1-3, curve={height=-12pt}, dashed]
	\arrow["{s}", from=1-3, to=2-2]
	\arrow[from=2-2, to=3-3]
	\arrow[Rightarrow, "{\eta}", from=0, to=2-2, shift left=1, shorten <=3pt, shorten >=3pt, dashed]
\end{tikzcd}\]
Where we see that the map $R \to R$ is already determined, and hence the datum
of such a diagram is precisely the homotopy $\eta$. Thus since $R \rar{s} R
\oplus I[1]$ corresponds to the map $ 0 \to I[1]$ in, such a lift is the same as
a nullhomotopy of the induced map:
\[ L_{S/k}\to I[1]\]
The set of homotopy classes of nullhomotopies form a \textit{torsor} over
$\rm{Map}(L _{S/k}, I[1])$
Altogether:
\begin{itemize}
\item Square zero extenstions $ I \to \widetilde{R}\to R$ are classified by:
  \[ \pi_o\rm{Map}_{D(A)}(L_{R/k}, I[1])\]
\item For $S \to R$ there is an obstruction in
  \[ \pi_0\rm{Map}_{D(S)}(L_{S/k}, I[1])\]
  for the existence of lifts $S \to \widetilde{R}$
\item Lifts (if they exist) are parametrized by:
  \[ \pi_1\rm{Map}_{D(S)}(L_{S/k}, I[1]) \simeq \pi_0 \rm{Map}(L_{S/k}, I)\]
  This is the classical statement that the lifts are a torsor under the group of derivations.
\end{itemize}
\begin{ex}
For a $k$-algebra $S$ the following are equivalent:
\begin{enumerate}
\item For every square zero extension $\widetilde{R}\to R$ and every map $S \to
  R$ there is a lift $S \to \widetilde{R}$
  \item $H_1L_{S/k} = 0$ and $H_0 L_{S/k}$ is a projective $S$-module.
\end{enumerate}
\end{ex}
\begin{theorem}
  If $R_1/\F_p$ is a perfect $F_p$-algebra then there exist flat
  $\Z/p^n$-algebras $R_n$, unique up to isomorphism, with $R_1 \simeq R_n
  \otimes_{\Z/p^n} \F_p$.
\end{theorem}
In particular $R_{n-1} \simeq R_n
  \otimes_{\Z/p^n}\Z/p^{n-1}$ and hence we get a diagram
  \[ \dots \to R_n \to \dots \to R_2 \to R_1\]
  with limit $R :=\flim_n R_n$ the unique flat, $p$-complete $\Z_p$-algebra with:
  \[ R_1 \simeq R\otimes _{\Z_p}\F_p = R/p\]
\begin{proof}
We can characterize $R_n$ as a non-split square zero extension of $R_{n-1}$ by
$R_1$. Namely by tensoring the exact sequence:
\[ 0 \to \Z/p \to \Z/p^n \to \Z/p^{n-1}\to 0\]
over $\Z/p^n$ with $R_n$ obtaining:
\[ 0 \to R_1\to R_n \to R_{n-1}\to 0\]
These are classified by \[\pi_0\rm{Map}_{D(R_{n-1})}(L_{R_{n-1}/\Z}, R_1[1])\]
which need to be compatible with an element of
\[\pi_0\rm{Map}_{D(\Z/p^{n-1})}(L_{\Z/p^{n-1}/\Z}. \Z/p[1])\]
classifying $\Z/p^n \to /Zp^{n-1}$
\begin{lemma}
  Isomorphism classes of such $R_n$ are a torsor over the group:
  \[ \pi_0 \rm{Map}_{D(R_{n-1})}(L_{R_{n-1}/\Z/p^{n-1}}, R_1[1])\simeq \pi_0
    \rm{Map}_{D(R_1)}(L_{R_1/\F_p}, R_1[1])\]
\end{lemma}
\begin{lemma}
If $R_1/\F_p$ is perfect then $L_{R_1/\F_p}\simeq 0$
\end{lemma}
\begin{proof}
  The map:
 \begin{align*}
   \phi: A &\to A\\
   x &\mapsto x^p 
 \end{align*}
 induces the $0$-map on $\Omega^1_{A/\F_p}$ (Exercise). By resolving $R_1$ via
 polynomial rings and using the naturality of the Frobenius we see that $\phi$
 acts by $0$ on $L_{R_1/\F_p}$, but since $A$ was perfect it also acts by an isomorphism.
\end{proof}
Hence there is a unique such lift and we are done.
\end{proof}
The $R$ form the theorem is called the (p-typical)\textit{Witt vectors} of $R_0$
written $W(R_0)$.
\begin{example}
For $\F_{p^n}/\F_p$ lifts to a flat $\Z_p$-algebra $W(\F_{p^n})$ with
$W(\F_{p^n})/p \simeq \F_p$. 
\end{example}
\section{Hochschild Homology of Schemes}
Throughout this section $X$ is a scheme over a ring $k$. We want to define
$HH(X/k)$. There are two ways to do this:
\begin{enumerate}
\item Extend from the affine case
\item Generalize $HH(\blank/k)$ to dg-categories over $k$ and define:
  \[ HH(X/k)= HH(\rm{Perf}(X)/k)\]
  The ``non -commutative'' approach.
\end{enumerate}
We will adapt the first approach.
\begin{definition}
  We define:
  \[ HH(X/k)= \lim_{U \subseteq X \textnormal{ open affine }}HH(\cl{O}(U)/k)\in D(k)\]
  I.e.~the limit over the functor:
  \[ HH(\cl{O}(\blank)/k): \{ U\subseteq X\textnormal{ affine open }\}\op \to D(k)\]
\end{definition}
Note that:
\[ HH(\spec(R)/k)= \lim_{U \subseteq \spec(R)}HH(\cl{O}(U)/k)=
  HH(\cl{O}(\spec(R))/k)= HH(R/k)\]
since $\spec(R)$ is initial in the category of affine opens.
\begin{example}
  Consider the projective space $\bb{P}^1_k$, this has an open cover:
  \[ (\bb{A}^1_k)^+ \cup (\bb{A}^1_k)^- = \bb{P}^1_k\]
  with intersection:
  \[ (\bb{A}^1_k)^+ \cap (\bb{A}^1_k)^- = \bb{G}_m \]
  Do we have some descent/Mayer-Vietoris principle?
\end{example}
\begin{theorem}
  For any pair $U,V \subset X$ open such that $X = U \cap V$ the square:
  \[\begin{tikzcd}
	{HH(X/k)} & {HH(U/k)} \\
	{HH(V/k)} & {HH(U \cap V/k)}
	\arrow[from=2-1, to=2-2]
	\arrow[from=1-1, to=2-1]
	\arrow[from=1-1, to=1-2]
	\arrow[from=1-2, to=2-2]
\end{tikzcd}\]
is a pullback in the derived category $D(k)$ i.e.~Hochschild Homology satisfies
Zariski descent.
\end{theorem}
\begin{remark}
We will see that $HH(\blank/k)$ satisfies even fpqc descent.
\end{remark}
\begin{corollary}
  In the situation of the theorem we get a long exact sequence:
  \[\begin{tikzcd}
	& {\dots} & {HH_{n+1}(U \cap V)} \\
	{HH_n(X)} & {HH_n(U) \oplus HH_n(V)} & {HH_n(U \cap V)} \\
	{HH_{n-1}(X)} & {\dots}
	\arrow[from=2-1, to=2-2]
	\arrow[from=2-2, to=2-3]
	\arrow[from=2-3, to=3-1]
	\arrow[from=3-1, to=3-2]
	\arrow[from=1-3, to=2-1]
	\arrow[from=1-2, to=1-3]
\end{tikzcd}\]
\end{corollary}
\begin{ex}
  For any commutative ring $R$ and $ x\in R$ we have that:
  \[ HH(R[[x^{-1}]/k])\simeq HH(R/k)\otimes_R R[x^{-1}]= HH(R/k)[x^{-1}]\]
\end{ex}
\begin{example}
  We have for $\bb{P}^!_k$ the pullback square:
  \[\begin{tikzcd}
	{HH(\bb{P}^1/k)} & {HH(k[x]/k)} & {x} \\
	{HH(k[y]/k)} & {HH(k[x^{\pm}]/k)} & {x} \\
	{y} & {x^{-1}}
	\arrow[from=1-1, to=1-2]
	\arrow[from=1-2, to=2-2]
	\arrow[from=1-1, to=2-1]
	\arrow[from=2-1, to=2-2]
	\arrow[from=1-3, to=2-3, maps to]
	\arrow[from=3-1, to=3-2, maps to]
\end{tikzcd}\]
And hence the Mayer Vietoris sequence:
\[\begin{tikzcd}
	{0} & {HH_1(\bb{p}^1/k)} & {k[x]dx \oplus k[y]dy} & {k[x^{\pm}]dx} \\
	& {HH_0(\bb{P}^1/k)} & {k[x]\oplus k[y]} & {k[x^{\pm}]} \\
	& {HH_{-1}(\bb{P}^1/k)} & {0}
	\arrow[from=3-2, to=3-3]
	\arrow[from=2-4, to=3-2]
	\arrow[from=2-2, to=2-3]
	\arrow[from=2-3, to=2-4, shift left=1]
	\arrow[from=1-4, to=2-2]
	\arrow[from=1-3, to=1-4]
	\arrow[from=1-2, to=1-3]
	\arrow[from=1-1, to=1-2]
\end{tikzcd}\]
With the map:
\begin{align*}
  k[x]\oplus k[y]& \to k[x^\pm]\\
  (f,g) &\mapsto f(x)+f(x^{-1})
\end{align*}
which is clearly surjective, hence $HH_{-1}(\bb{P}^1/k)=0$. Moreover we have the
map:
\begin{align*}
  k[x]dx \oplus k[y]dy &\to k[x^\pm]dx\\
  (fdx, gdy) &\mapsto f(x)dx + \underbrace{g(x^{-1})d(x^{-1})}_{=-\frac{g(x^{-1})dx}{x^2}}
\end{align*}
Which has a one-dimensional cokernel and trivial kernel. Hence we have
$HH_0(\bb{P}^1/k)= k \oplus k$ and $HH_1(\bb{P}^1/k)=0$
\end{example}
\begin{proof}[Proof of the Theorem]
  Have $U,V \subseteq X$ open and want to show that we have a pullback:
  \[\begin{tikzcd}
	{\lim_{A \subseteq X}HH(A) \simeq HH(X)} & {HH(U)\simeq \lim_{A \subseteq X} HH(A \cap U)} \\
	{\lim_{A\subseteq X} HH(A \cap V) \simeq HH(V)} & {HH(U \cap V) \simeq \lim_{A \subseteq X}HH(A\cap U\cap V)}
	\arrow[from=2-1, to=2-2]
	\arrow[from=1-2, to=2-2]
	\arrow[from=1-1, to=2-1]
	\arrow[from=1-1, to=1-2]
\end{tikzcd}\]
Hence since limits commute with limits we can assume that $X$ is affine.
Moreover one can assume that $U,V$ are affine and standard open (not so clear). Thus we reduce
to the case:
\[\begin{tikzcd}
	{HH(R/k)} & {HH(R[x^{-1}]/k)} \\
	{HH(R[y^{-1}])} & {HH(R[x^{-1}, y^{-1}]/k)}
	\arrow[from=1-1, to=1-2]
	\arrow[from=1-1, to=2-1]
	\arrow[from=2-1, to=2-2]
	\arrow[from=1-2, to=2-2]
\end{tikzcd}\]
For this we get the diagram:
\[\begin{tikzcd}
	{HH(R/k)} & {HH(R/k) \otimes_R R[x^{-1}]} \\
	{HH(R/k) \otimes_R R[y^-1]} & {HH(R/k) \otimes_R R[x^{-1}, y^{-1}]}
	\arrow[from=1-1, to=1-2]
	\arrow[from=1-1, to=2-1]
	\arrow[from=2-1, to=2-2]
	\arrow[from=1-2, to=2-2]
\end{tikzcd}\]
To see that this is a pullback it suffices to prove that:
\[\begin{tikzcd}
	{R} & {R[x^{-1}]} \\
	{R[y^{-1}]} & {R[x^{-1}, y^{-1}]}
	\arrow[from=1-1, to=1-2]
	\arrow[from=1-1, to=2-1]
	\arrow[from=2-1, to=2-2]
	\arrow[from=1-2, to=2-2]
\end{tikzcd}\]
is  a pullback. However its clearly a pushout and hence a pullback by stability.
\end{proof}
\underline{Recall}: If $R$ has flat cotangent complex (i.e.~cotangent complex
concentrated in degree 0 given by a flat $R$-module) then we have that:
\[ HH_{\pt}(R/k)\simeq \Omega^{\pt}_{R/k}\]
In general we had a filtration $F^{\pt}_{\rm{HKR}}HH(R/k)$ with $n$-th associated
  graded is given by:
  \[ F^{n+1}_{\rm{HKR}}\to F^n_{\rm{HKR}} \to L \Omega^n_{R/k}[n] = (L\Lambda^n)(L_{R/k})[n]\]
  This filtration is complete in the sense that:
  \[ \flim F^{\pt}_{HKR}= 0 \in D(k)\]
  We define a similar filtration on $HH(X/k)$ via:
  \[F^n_{\rm{HKR}}(X/k) := \lim_{U \subseteq X \textnormal{ affine open }} F^{n}_{\rm{HKR}}(HH(\cl{O}(U)/k))\]
  \begin{proposition}
    This defines a complete filtration on $HH(X/k)$ i.e.we have that:
    \[ \flim_n F^{n}_{\rm{HKR}}(X/k)= 0\]
    And the $n$-th associated graded is given by:
    \[ \lim_{U \subseteq X}L \Omega_{U/k}^n[n]= R\Gamma(X;L\Omega^n_{\cl{O}/k}[n])\]
    Which is called the derived Hodge cohomology of $X$.
  \end{proposition}
  \begin{remark}
    Note that if $X$ is smooth we have $L\Omega= \Omega$ i.e.:
    \[ R\Gamma(X; \Omega^n_{\cl{O}/k}[n])\]
    is just the sheaf cohomology of the sheaf of K\"ahler differentials, which
    is known as Hodge cohomology.
  \end{remark}
  \begin{proof}
    The completeness is clear since limits commute with limits. The second claim
    follows since cofibers commute with limits.
  \end{proof}
  In particular we get a spectral sequence:
  \[ R^{i}\Gamma(X; L\Omega^n_{\cl{O}/k}[n])\implies HH_{\pt}(X/k)\]
  \begin{corollary}
    If $ \Q \subseteq k$ then:
    \[ HH(X/k)\simeq \prod_{}^{}R\Gamma(X; L\Omega^n_{\cl{O}/k}[n])\]
  \end{corollary}
  \begin{proof}
    We have seen this in the case $X = \spec(k[x_1, \dots, x_n])$. Hence it
    follows for any affine $X$ by non-abelian derivation. The general case
    follows(?).
  \end{proof}
  \section{Spectra}
  We write $\cl{S}_{\pt}$ for the category of pointed spaces. We have two
  functors:
  \[ \Sigma, \Omega : \cl{S}_{\pt}\to \cl{S}_{\pt}\]
  defined via:
  \[\begin{tikzcd}
	{X } & {\pt} \\
	{\pt} & {\Sigma X}
	\arrow[from=1-1, to=1-2]
	\arrow[from=2-1, to=2-2]
	\arrow[from=1-1, to=2-1]
	\arrow[from=1-2, to=2-2]
\end{tikzcd}\]
\[\begin{tikzcd}
	{\Omega X} & {\pt} \\
	{\pt} & { X}
	\arrow[from=1-1, to=1-2]
	\arrow[from=2-1, to=2-2]
	\arrow[from=1-1, to=2-1]
	\arrow[from=1-2, to=2-2]
\end{tikzcd}\]
\begin{definition}
  A \textit{Spectrum} is a sequence of spaces $X_i$ where $i \geq 0$ together
  with equivalences $X_i \rar{\sim} \Omega X_{i+1}$. We define the category of
  spectra as:
  \[ \rm{Sp}:=
    \rm{Eq}(\cl{S})_{\pt}\times_{\cl{S}_\pt}\rm{Eq}(\cl{S}_\pt)\times_{\cl{S}_{\pt}}\dots \]
  where $\rm{Eq}(\cl{S}_{\pt})$ is the full subcategory of
  $\cl{S}_\pt^{\Delta^!}$ consisting of those maps which are equivalences. The
  pullback is formed along the maps:
  \[\begin{tikzcd}
	& {\rm{Eq}(\cl{S}_\pt)} \\
	{\rm{Eq}(\cl{S}_\pt)} & {\cl{S}_\pt}
	\arrow["{\rm{target}}"', from=2-1, to=2-2]
	\arrow["{\Omega\circ \rm{source}}", from=1-2, to=2-2]
\end{tikzcd}\]
\end{definition}
Note that objects $\rm{Sp}$ are indeed spectra. Moreover a map consists of a
list of maps $X_i \rar{f_i} Y_i$ and choices of homotopies in the squares:
\[\begin{tikzcd}
	{X_i} & {Y_i} \\
	{\Omega X_{i+1}} & {\Omega Y_{i+1}}
	\arrow["{f_i}", from=1-1, to=1-2]
	\arrow[from=1-2, to=2-2]
	\arrow[from=1-1, to=2-1]
	\arrow["{\Omega f_{i+1}}"', from=2-1, to=2-2]
	\arrow[Rightarrow, from=1-2, to=2-1, shorten <=5pt, shorten >=5pt]
\end{tikzcd}\]
\begin{remark}
  Alternatively we have that:
  \[ \rm{Sp} \simeq \lim ( \dots \rar{\Omega} \cl{S}_\pt \rar{\Omega}
    \cl{S}_\pt) \in \rm{Cat}_{\infty}\]
  and moreover:
  \[ \rm{Map}_{\rm{Sp}}(X, Y) \simeq \rm{lim}(\rm{Map}_{\cl{S}_\pt}(X_i, Y_i))\]
\end{remark}
\begin{example}
  \begin{itemize}
    \item $(HA)_i = K(A, i)$ with equivalences $K(A, i-1)\rar{\sim} \Omega K(A,i)$
      \item $X\in \cl{S}_\pt$ the \textit{Suspension spectrum} of $X$ is given
        by:
        \[ (\Sigma^{\infty}X)_i= \colim_k \Omega^k\Sigma^{k+i}X\]
        in particular $\Sigma^{\infty}S^0=: \S$ is the \textit{Sphere Spectrum}
  \end{itemize}
\end{example}
\begin{lemma}
  We have an equivalence:
  \[ \rm{Map}_{\rm{Sp}}(\Sigma^{\infty}X, Y) \simeq \rm{Map}_{\cl{S}_\pt}(X, Y_0)\]
\end{lemma}
\begin{proof}[Proof sketch]
We have $X \to Y_0 \simeq \Omega^{k+i} Y_{k+i}$ is adjoint to $\Sigma^{k+i}X \to
Y_{k+i}$. Now applying $\Omega^k$ we get a map:
\[ \Omega^k \Sigma^{k+i}X \to Y_i\]
inducing a map on the colimit:
\[ (\Sigma^{\infty}X)_i = \colim_k \Omega^k \Sigma^{k+i} X \to Y_i\]
\end{proof}
\begin{definition}
  We define the \textit{infinite loop space functor} as:
  \begin{align*}
    \Omega^{\infty}: &\rm{Sp}\to \cl{S}_\pt\\
    &Y \mapsto \Omega^{\infty}Y = Y_0
  \end{align*}
\end{definition}
Thus the previous lemma says that we have an adjunction $\Sigma^{\infty} \dashv \Omega^{\infty}$
\begin{example}
  For $K \in \cl{S}_\pt$ \textit{finite} we have that:
  \begin{align*}
   \rm{Map}_{\rm{Sp}}( \Sigma^{\infty}K, \Sigma^{\infty}X) &\simeq
                                                             \rm{Map}_{\cl{S}_\pt}(K, \Omega^{\infty}\Sigma^{\infty}X)\\ 
                                                           &=\rm{Map}_{\cl{S}_\pt}(K, \colim_k \Omega^k\Sigma^k X)\\
                                                           &\simeq \colim_k\rm{Map}_{\cl{S}_\pt}(K, \Omega^k\Sigma^kX)\\
                                                           &\simeq \colim_k \rm{Map}_{\cl{S}_\pt}(\Sigma^kK, \Sigma^kX)
  \end{align*}
  In particular for $K= S^0$ we get that:
  \[\pi_n \rm{Map}(\S, \Sigma^{\infty}X)= \pi^s_nX\]
\end{example}
\begin{lemma}
  $\rm{Sp}$ has all limits, filtered colimits and a zero object.
\end{lemma}
\begin{proof}
A zero object is given by $X_i =\pt$. Limits and filtered colimits commute with
$\Omega$ since it is a pullback. Thus by the limit formula we gave for mapping
spaces these exist and are computed pointwise.
\end{proof}
\begin{lemma}
The functor $\Omega: \rm{Sp}\to \rm{Sp}$ is an equivalence.
\end{lemma}
\begin{proof}
  Since limits are computed pointwise takes a spectrum $(X_0,X_1, \dots )$ to
  $(\Omega X_0, X_0, X_1, \dots)$, thus the inverse is given by
  shifting in the other direction.
\end{proof}
\begin{proposition}
  $\rm{Sp}$ has all colimits and pushout squares are pullback squares and vice versa.
\end{proposition}
\begin{proof}
  Any $\infty$-category with finite limits, a zero object and $\Omega$ is an
  equivalence has pushouts and these are determined by the fact that pushout
  squares $=$ pullback squares (Omitted). Once we have pushouts we are done
  since we already have filtered colimits.
\end{proof}
Note that $\Sigma: \rm{Sp} \to \rm{Sp}$ is inverse to $\Omega$ it is given by
shifting to the left. In partiuclar it  is \textit{not} computed pointwise.
Moreover note that we have:
\[ Y_i = \Omega^\infty \Sigma^i Y\]
\begin{example}
  We have that:
  \[ \left[ \Sigma^\infty X , \Sigma^n HA \right]_{\rm{Sp}} = \left[ X, K(A,n)
    \right]_{\cl{S}_\pt}= H^{n}(X;A)\]
  and similarly:
  \[ [ \Sigma^n \S, Y]_{\rm{Sp}}= [S^n, \Omega^{\infty}]_{\cl{S}_\pt}=
    \pi_n(\Omega^\infty Y)\]
\end{example}
\begin{lemma}
  For every $Y \in \rm{Sp}$ we have that:
  \[ Y \simeq \colim_i \Sigma^{-i}\Sigma^\infty\]
  where we denote $\Sigma^{-i}= \Omega^{i}$
\end{lemma}
\begin{proof}
  We have that:
 \begin{align*}
   \rm{Map}_{\rm{Sp}}(Y,Y)&\simeq \lim_i \rm{Map}_{\cl{S}_\pt}(Y_i, Z_i)\\
   &\simeq \lim_i \rm{Map}_{\rm{Sp}}(\Sigma^{\infty}Y_i, \Sigma^iZ)\\
   &\simeq \lim_i \rm{Map}_{\rm{Sp}}(\Sigma^{-i}\Sigma^{\infty}Y_i, Z)\\
   &\simeq \rm{Map}_{\rm{Sp}}(\colim_i \Sigma^{-i}\Sigma^{\infty}, Z)
 \end{align*}
\end{proof}
\underline{Construction}:
Denote the singular chains of a space $X$ by $C_\pt(X)$ then we have a functor:
\begin{align*}
  \rm{Sp} &\to D(\Z)\\
  Y &\mapsto \colim_i C_\pt(Y_i)[-i]
\end{align*}
which preserves colimits and maps $\Sigma^\infty X $ to $C_\pt(X)$ so in
particular $\S \mapsto \Z[0]$.
\begin{definition}
Let $\rm{Sp}^{\rm{fin}}\subseteq \rm{Sp}$ be the full subcategory of objects of
the form:
\[ \Sigma^{-n}\Sigma^{\infty}K \quad \textnormal{ with } \quad X \in \cl{S} \textnormal{finite }\]
\end{definition}
\begin{exercise}
  This is closed under finite colimits.
\end{exercise}
In fact this can be described as the subcategory generated by $\S$ under finite
colimits. Moreover we have the following:
\begin{proposition}
  $\rm{Sp}$ is freely generated under filtered colimits by $\rm{Sp^{\rm{fin}}}$
  i.e.~$\rm{Sp}= \rm{Ind}(\rm{Sp}^{\rm{fin}})$
\end{proposition}
\begin{proof}
  We get a functor from the universal property of Ind. Moreover
  $\rm{Sp}^{\rm{fin}}$ in $\rm{Sp}$ is compact since:
  \begin{align*}
    \rm{Map}_{\rm{Sp}}(\Omega^{\infty}K, \fcolim_j Y^j) &\simeq \rm{Map}_{\cl{S}_\pt}(K, \Omega^{\infty} \Sigma^n \fcolim_j Y^j)\\
    &\simeq \fcolim_j\rm{Map}_{\cl{S}_\pt}(K, \omega^{\infty} \Sigma^n Y^j)
  \end{align*}
  where we have used that filtered colimits are computed pointwise. Now write:
  \[ Y = \colim_i \Sigma^{-i}\Sigma^\infty Y_i\]
  and then each $Y_i$ as a filtered colimit of finite spaces which is always possible.
\end{proof}
\noindent
\underline{Analogy}:
Have $ D^{\rm{perf}}(R)\subseteq D(R)$ the full subcategory of perfect complexes
and we have:
\[ D(R)= \rm{Ind}(D^{\rm{perf}}(R))\]
so we want to think of $\rm{Sp}$ as ``$D(\S)$'' however additional subtleties
arise from the fact that $\rm{Map}(\S, \S)$ is not discrete. Moreover think of
the singular chains functor $C_\pt: \rm{Sp}\to D(\Z)$ as the ``basechange  along
$\S \to \Z$''
\begin{definition}
$X\in \rm{Sp}$ is called $n$-connective if $\pi_iX =0 $ for $i<n$ and
$n$-coconnective if $\pi_i X=0$ for $i>n$ and denote the corresponding full
subcategories as $\rm{Sp}_{\geq n}$ and $\rm{Sp}_{leq n}$ respectively.
\end{definition}
\begin{proposition}
  The inclusion $\rm{Sp}_{\leq n} \rari{} \rm{Sp}$ has a left adjoint denoted
  $\tau_{\leq n}$.
\end{proposition}
\begin{proof}
  $X_n = X$, buo;d $X_i$ as follows:\\
  Pick generators of $\pi_i(X_{i-1})$ which induce a map:
  \[ \bigoplus \Sigma^i \S\to X_{i-1} \]
  and define $X_i$ as the cofiber. Then set $\tau_{\leq n} X:= \colim_i X_i$,
  then we have:
  \begin{itemize}
    \item $\tau_{\leq n}X \in \rm{Sp}_{\leq n}$ and $\pi_i \tau_{\leq n}X= \pi_i
      X$ by the long exact sequence on homotopy groups and using fact that $\S$
      is connective.
      \item Every map $X \to Y \in \rm{Sp}_{\leq n}$ factor over these cofibers
        and thus over $\tau_{\leq n}X \to Y$
  \end{itemize}
\end{proof}
\begin{definition}
  We denote by $\tau_{\leq n}$ be the fiber of the map $X \to \tau_{\leq n-1}X$
\end{definition}
\begin{lemma}
\begin{enumerate}
\item For $X\in \rm{Sp}_{\geq n}$ and $Y \in \rm{Sp}_{n-1}$ we have $\rm{Map}(X,
  Y) \simeq 0$
  \item We have an equivalence of categories $\rm{Sp}_{\geq n}\cap \rm{Sp}_{\leq
    n}\simeq \rm{Ab}$
\end{enumerate}
\end{lemma}
\begin{proof}
Any map $X \to Y$ factors through $\tau_{\leq n-1}X \simeq 0\to Y$ which shows
1. For 2. obeserve that $\pi_n$ gives a functor and that mapping spaces are
discrete since:
\[\pi_i \rm{Map}(X,Y)\simeq \rm{Map(\Sigma^iX, Y)}\simeq 0 \quad \textnormal{
    for } i>0\]
If $\pi_nX$ is free then we can choose a map:
\[ \bigoplus \Sigma^n \S \to X\]
which is an iso on $\pi_n$ and thus an equivalences under $\tau_{\leq n}$. If
it is not free chose a map $bigoplus \Sigma^n \S \to X$ which is surjective of
$\pi_n$, then the fiber has free $\pi_n$. Use the exact sequence for
$\pi_n\rm{Map}(\blank, Y)$ (??)
\end{proof}
\underline{Postnikov Tower:}\\
For every $X$ we have a tower:
\[\begin{tikzcd}
	&& {X} \\
	{\dots } & {\tau_{\leq n+1}X} & {\tau_{\leq n }X} & {\tau_{\leq n-1}X} & {\dots} \\
	&& {H(\pi_{n}X)[n+1]}
	\arrow[from=1-3, to=2-4]
	\arrow[from=1-3, to=2-3]
	\arrow[from=1-3, to=2-2]
	\arrow[from=2-4, to=2-5]
	\arrow[from=2-1, to=2-2]
	\arrow[from=2-2, to=2-3]
	\arrow[from=2-3, to=2-4]
	\arrow["{\rm{fib}}", from=3-3, to=2-3]
\end{tikzcd}\]

