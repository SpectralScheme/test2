\section{Local Fields}
\subsection{Generalities}
In the following let $K$ be a field and $\abs{ \blank}:K^{\times}\to
\bb{R}^{\times}$ an non-archimedian absolute value on $K$. This absolute value
induces an ultrametric on $K$ which turns it into a topological field. Hence we
have the following:
\begin{enumerate}
\item Open balls in $K$ are either disjoint or equal $\implies$ $K$ is totally disconnected.
  \item $Z \subseteq K$ is bounded
    \item If $K$ is complete then the usual facts about convergence of sequences in $\bb{C}$ hold.
\end{enumerate}
\begin{definition}
The absolute value $\abs{\blank}$ is called \textit{discrete} if
$\abs{K^\times}\subseteq \bb{R}^\times$ is discrete, i.e.~given by $\rho^\Z$ for
some $\rho > 1$. The \textit{discrete valuation} induced by $\abs{\blank}$ is
the map:
\begin{align*}
  \nu : &K^{\times} \to \Z\\
                     &x \mapsto -\log_\rho(x)
\end{align*}
i.e.~$\nu(x)$ is the integer such that:
\[ \abs{x}= \rho^{-\nu(x)}\]
\end{definition}
\begin{proposition}
  Any discrete valuation $\nu$ satisfies:
  \begin{enumerate}
  \item $\nu$ is surjective
    \item $\nu(xy)= \nu(x) +\nu(y)$
      \item $\nu(x+y)\geq \min((\nu(x), \nu(y)))$
  \end{enumerate}
\end{proposition}
\begin{proof}
Clear from the properties of the absolute value and the logarithm.
\end{proof}
Given a discrete valuation $\nu: K^{\times}\to \Z$ one can always produce an
absolute value by picking any $\rho > 1$ and setting:
\[ \abs{x}_\rho:= \rho^{-\nu(x)}\]
Note that for a different choice $\rho\p$ we have that:
\[ \abs{x}_{\rho\p} = \abs{x}_\rho^c\]
for $c = \log_{\rho\p}(\rho)$ hence these norms are equivalent. Thus topology
of $K$ only depends on the discrete valuation $\nu$ which makes it more
convenient for studying $K$ as a topological field. Its is also called a
\textit{place} of $K$\\
To get anywhere with our theory we need to restrict ourselves to complete valued
fields (otherwise we're still inlcuding $\Q$ even):
\begin{definition}
The Cauchy completion of $K$ (as a metric space) naturally inherits the
structure of a valued field denoted $\widehat{K}$.
\end{definition}
Here's a more algebraic construction: \\
Suppose $K$ is the quotient field of some Dedekind ring $R$. Then each prime
$\frak{p}$ of $R$ gives rise to a discrete valuation as follows:\\
For $z\in K$ write $z = \frac{x}{y}$ with $x, y\in R$. Since $R$ is Dedekind the
ideals $xR,yR$ decompose uniquely into primes. Denote by $\nu(x),\nu(y)\in
\Z_{\geq 0 }$ the power of $\frak{p}$ appearing in these decompositions. Then:
\[ \nu(z)= \nu\left(  \frac{x}{y}\right) = \nu(x)- \nu(y)\]
defines a discrete valuation on $K$. In this setup we can first $p$-adically
complete $R$ i.e.~take the limit:
\[ R_{\frak{p}}^{\wedge}:=\flim_{n}R/\frak{p}^n\]
Then we recover the Completion $\widehat{K}$ as the quotient field of $R_{\frak{p}}^{\wedge}$.
\begin{example}
One should always keep in mind the case $R= \Z$ and $K = \Q$. In this case
Ostrowski's Theorem shows that the places of $\Q$ are in bijection to the primes
of $\Z$ under this construction. The $p$-adic completion of $\Z$ is of course
$\Z_p$ and we have $\rm{Quot}(\Z_p)=\Q_p$
\end{example}
From now on we additionally assume that $(K, \nu)$ is complete.
\begin{definition}
The discrete valuation ring $\cl{O}_K$ of the valued field $(K, \nu)$ is defined as:
\[ \cl{O}_K:= \left\{x \in K \mid \nu(x) \geq 0\right\}_{}\]
This is a local PID with maximal ideal:
\[\frak{m}_K := \left\{x \in K \mid \nu(x) \geq 1\right\}_{}\]
We call the quotient $\kappa_K:= \cl{O}_K/\frak{m}$ the \textit{residue field}
of $K$. Often we implicitly choose a generator of $\frak{m}$ which we denote by $\pi_K$.
\end{definition}
Note that $\cl{O}_K$ is now always a Dedekind Domain such that
$\rm{Quot}(\cl{O}_K) = K$ where the valuation $\nu$ corresponds to the unique
prime $\frak{m}$ of $\cl{O}_K$ under the construction explained earlier. Hence
for $x\in \cl{O}_K$ the valuation $\nu(x)$ is the natural number such that:
\[ x\cl{O}_K = \frak{m}^{\nu(x)}\]
and we extend this to all of $K$ such that it becomes a group homomorphism.\\
We want to study algebraic extension of $K$ which relies crucially on the
following result:
\begin{theorem}
Let $L/K$ be an algebraic extension, then there exists a unique discrete
valuation on $L$ extending $\nu$.
\end{theorem}
\begin{proof}
  First note that because of the uniqueness we may assume that $L/K$ is finite.
  Moreover we prove the slightly stronger statement, that any choice of norm
  $\abs{\blank}$ corresponding to $\nu$ this extends uniquely to $L$. For
  existence one gives an explicit construction:
 Recall that for $x \in L$ the \textit{Norm} of $x$ was
  defined as:
  \[ N_{L/K}(x):= \prod_{\sigma: K(x)\rari{} \bar{K}}^{} \sigma(x)^{\deg(L/K(x))}\]
  Then one can check that:
  \[ \abs{x} := \sqrt[n]{\abs{N_{L/K}(x)}} \qquad n = \deg(L/k)\]
  defines a norm as desired. (The strong triangle inequality is tricky).
 Now any choice of absolute value extending $\abs{\blank}$ induces a norm on $L$
 after choosing a basis. Since all norms on finite dimensional vector spaces are
 equivalent the claim follows since any two norms must be powers of one another. 
\end{proof}
To study algebraic extensions of $K$ we of course need to understand the
polynomial ring $K[t]$. One thing one immediately sees is that since $\cl{O}_K$
is a PID we can content ourselves with studying $\cl{O}_K[t]$ in some sense by the
Gauss Lemma. Because we are complete i.e.:
\[ \cl{O}_K \simeq \flim_n \cl{O}_K / \frak{m}^n\]
we can reduce questions about $\cl{O}_K[t]$ to questions about $\kappa[t]=
\cl{O}/\frak{m}[t]$ in the following way:
\begin{theorem}[Hensels Lemma]
Let $f \in \cl{O}_K[t]$ be a polynomial such that there
are coprime polynomials $g_0,h_0 \in \kappa[t]$ with $g_0$ monic and:
\[ \bar{f} = g_0h_0 \in \kappa[t]\]
(i.e.~$f$ splits ``nicely'' into irreducibles modulo $\frak{m}$). Then we can lift
this factorization, namely there are $g, h\in \cl{O}_K[t]$ with $g$ monic and:
\[ f= gh \in \cl{O}_K[t]\]
\end{theorem}
\begin{remark}
\begin{itemize}
\item Note that this in some sense provides a converse to the fact that if $f$
  splits over $\cl{O}_K$ then it also splits modulo $\frak{m}$
  \item The assumption that $g_0,h_0$ be irreducible is often irrelevant, since one can
    decompose further into irreducibles and apply the theorem inductively
    \item The most important special case is the one where $g_0 = t - \alpha_0
      \in \kappa[t]$ is linear. Then since the lift $g$ must also monic it is
      also linear. Thus if $f$ has a root modulo $\frak{m}$ it also has one over
      $\cl{O}_K$. More can be said:
\end{itemize}
\end{remark}
\begin{corollary}
  If $f \in \cl{O}_K[t]$ is separable then the reduction modulo $\frak{m}$ defines
  a bijection between the roots of $f$ and the roots of $\bar{f} \in \kappa[t]$
\end{corollary}
\begin{definition}
A \textit{local field} is a complete non-archimedian discretely valued field
with finite residue field.
\end{definition}
A local field comes with a ``natural'' choice of absolute value, since $\kappa =
\F_q$ for some prime power $q$ we set:
\[ \abs{x}:= q^{-\nu(x)}\]
We also have the following observation:
\begin{proposition}
  $K$ has all roots of unity of order dividing $q-1$.
\end{proposition}
\begin{proof}
This is immediate from Hensels Lemma and the fact that $\bb{F}_q^{\times} \iso
\Z/(q-1)$ i.e.~$\kappa= \F_q$ has all roots of unity dividing $q-1$.
\end{proof}
\begin{proposition}
  Let $L/K$ be a finite extension, then $L$ is also local i.e.~$\kappa_L$ is finite and we have:
  \[ \frak{m}_K\cl{O}_L = \frak{m}_L^{\nu_L(\frak{m}_K)}\]
\end{proposition}
\begin{definition}
\begin{itemize}
\item $f(L/K):= \deg(\kappa_L/\kappa_K)$ is called the \textit{intertia degree}
  \item $e(L/K) := \nu_L(\frak{m}_K)$ is called the \textit{ramification index}
    \item $L/K$ is called unramified if $e(L/K)=1 $ and totally ramified if
      $e(L/K)= \deg(L/K)$
\end{itemize}
\end{definition}
\begin{example}
  For $K = \Q_p$ let $ p \neq \alpha \in \Z$, then $Q_p(\sqrt[n]{\alpha})$ is
  unramified (we will see this later) and moreover for $\beta$ a root of $X^n -
  p$ we have that $p$ decomposes in $\Q_p(\beta)$ as:
  \[ p = \beta^n\]
  and hence this is totally ramified of degree $n$.
\end{example}
\begin{proposition}
  We have that:
  \begin{enumerate}
  \item $ef= \deg(L/K)$ in particular $\deg(L/K)\geq \deg(\kappa_L/\kappa_K)$
    \item $\cl{O}_L$ is a free $\cl{O}_K$-module of rank $\deg(L/k)$ 
  \end{enumerate}
\end{proposition}
\begin{proof}
This is proven directly by constructing a basis $\{w_j\pi^i_L\}$ from the power
series expansion of an arbitrary element.
\end{proof}
There is a very important corollary to the proof of this proposition (which we
omitted):
\begin{corollary}\label{monogenic}
  Let $L/K$ be separable and totally ramified with $\deg(L/K)= n$. Then for any prime $\pi_L$ of $L$
  the set $\{1, \pi_L, \dots, \pi_L^{n-1}\}$ is an $\cl{O}_K$-basis of
  $\cl{O}_L$. In particular totally ramified extensions are monogenic (!!) and
  generated by their prime i.e.~$L=K(\pi_L)$
\end{corollary}
We've seen that ramification and inertia are in some sense opposite phenomena
which ``make up'' the degree of a field extension. In fact one can separate them
even more cleanly as the following proposition shows:
\begin{proposition}
Let $L/K$ be a finite extension. There is a unique intermediate field $K
\subseteq L_0 \subseteq L$ such that $K \subseteq L_0$ is unramified and $L_0
\subseteq K$ is totally ramified, called the \textit{inertia field} of the extension. 
Moreover we have:
\begin{enumerate}
\item $L_0= K(\zeta)$ for any root of unity $\zeta$ of order $\#\kappa_L -1$
  \item Any intermediate extension of $L/K$ which is unramified is contained in $L_0$
\end{enumerate}
\end{proposition}
\begin{proof}
Note that by what we showed earlier we necessarily have $K(\zeta)\subseteq L_0$,
so we are claiming the other inclusion. They key is to note that the inclusion
is trivial on residue fields. Indeed: Write $q:= \#\kappa_L$, we have that
$\kappa^\times_L$ is cyclic of order $q-1$ and hence generated by some root of
unity $\bar{\zeta}$ of order $q-1$ and thus we have:
\[\kappa_L =\kappa_K(\bar{\zeta})\] 
Using Hensels Lemma we can find a lift $\zeta \in \cl{O}_L$ of $\bar{\zeta}$.
Then we have:
\[ \deg(K(\zeta)/K)\geq \deg(\underbrace{\kappa_K(\bar{\zeta})}_{=
    \kappa_L}/\kappa_K)= f\]
Hence $K(\zeta)/K$ is unramified of degree $f$ and since $\deg(L/K)= ef$ it
follows that
$L/K(\zeta)$ is totally ramified. The second claim follows by the same arguments.
\end{proof}
\begin{corollary}
  The following hold:
  \begin{enumerate}
  \item The composite of unramified extensions is unramified
    \item If $L/K$ is any unramified extension then $L= K(\zeta)$ for $\zeta$
      some root of unity of order $\# \kappa_L -1$. Moreover $L/K$ is Galois
      with Galois Group:
      \[ G_{L/K} \iso G_{\kappa_L/\kappa_K}= \faktor{\Z}{f(L/K)}\]
  \end{enumerate}
\end{corollary}
\begin{proposition}
Every local field of characteristic 0 is a finite extension of $\Q_p$
\end{proposition}
\subsection{The unit group}
Throughout let $K/\Q_p$ be a finite extension. First note that:
\[ K^\times = \pi_K^\Z\times \cl{O}_K^\times\]
We want to study the group of units $U_K :=\cl{O}^\times_K$. We've already seen that
this contains the roots of unity $\mu_{q-1}$ but this inclusion is in fact
split. First some definitions:
\begin{definition}
  The $n$-th \textit{Higher Unit group} of $K$ is defined as:
  \[ U^{(n)}_K := \left\{x \in \cl{O}_K \mid x\equiv 1 \mod \frak{m}_k^n\right\}_{}\]
  Where by convention we set:
  \[ U^{(0)}_K := U_K\]
\end{definition}
Note that these are in particular units and we have a descending filtration:
\[ \dots \subseteq U_K^{(n)}\subseteq \dots \subseteq U_K^{(1)}\subseteq U_K^{(0)}=U_K\]
of open and closed subgroups such that $\bigcap_{n \geq 1} U_K^{(n)}= \{1\}$. We
will show next that we also understand the associated graded, but first note
that, in terms of powers series expansion, the elements of $U_K^{(n)}$ are
precisely those of the form:
\[ 1 + \sum_{i\geq n}^{} a_i \pi_K^i\]
This suggests already that their subquotients are cyclic of order $ q- 1=\#\kappa_K
-1$ and this is indeed the case:
\begin{lemma}
  The following hold:
  \begin{enumerate}
  \item $U_K \iso \mu_{q-1}\times U_k^{(1)}$
    \item $U_K/U_K^{(1)}\iso \kappa_K^{\times}= \F_q^{\times}\iso\Z/(q-1)$
    \item $U^{(n)}_K/U_K^{(n+1)}\iso (\F_q,+)$
      \item $U^{(n)}_K$ is compact for all $n$
  \end{enumerate}
\end{lemma}
\begin{proof}
  Consider the canonical surjection $\cl{O}^\times=U_K \to \kappa^\times$ this
  has kernel precisely $U^{(1)}_K$ and is split by the Teichm\"uller map,
  proving 1.~and 2.\\
  For 3.~observe that:
  \[(1+a\varpi^n)(1+b\varpi^n)= 1+(a+b)\varpi^n + ab\varpi^{2n}\]
  And hence:
  \begin{align*}
    U^{(n)}&\to (\F_q,+)\\
    1+a\varpi^n &\mapsto a~\rm{mod}~\frak{m}
  \end{align*}
  Defines a surjective group homomorphism with kernel precisely $U^{(n+1)}$.\\
  The statement 4.~follows since the $U^{(n)}$ are clearly closed in $\cl{O}$
  and the latter is compact since it is profinite.
\end{proof}
Note: It might be better to write $C_{q-1}$ instead of $\Z/q-1$, since it is also
multiplicatively written, there is no risk of confusing it with $\F_q$ and we
make no choice of generator.\\\\
There is one more major obstacle for computing $U_K$, the filtration $U^{(n)}_K$
clearly never becomes trivial! However it does stabilize in the following sense:
\begin{proposition}
  Let $e= e(L/K)$ and $n > \frac{e}{p-1}$, then the power series:
  \[ \exp(x) = \sum_{i\geq 1}^{} \frac{x^i}{i!} \quad \textnormal{ and } \quad \log(1+x) =
    \sum_{i \geq 1}^{}(-1)^{i+1} \frac{x^i}{i}\]
  define inverse isomorphisms of topological groups:
  \[\begin{tikzcd}
	{\mathfrak{m}_K^n} & {U_K^{(n)}}
	\arrow["{\exp}", from=1-1, to=1-2, shift left=1]
	\arrow["{\log}", from=1-2, to=1-1, shift left=2]
\end{tikzcd}\]
\end{proposition}
\begin{remark}
  \begin{itemize}
 \item It is clear that we can expect no such isomorphism on the level of
 $U_K$ since it contains torsion. In the real case one remedies this by
 restricting to positive numbers but this makes no sense here. 
 \item Since we are non-archimedian a series converges iff its terms converge to zero
   \item The series $\log(1+x)$ clearly converges for all $x \in \frak{m}_K$ and
     hence $\log(y)$ converges for all $y \in U^{(1)}_K$, meaning we always get
     a map of topological groups:
     \[ U_K^{(1)}\to \frak{m}_K\]
     which is compatible with the filtrations.
\item The exponential series has difficulties converging because the absolute value of
   $\frac{1}{i!}$ grows quite quickly, completely opposite to the real case.  
  \end{itemize}
\end{remark}
\begin{proof}
  Only left to check, that the exponential series converges under the given
  bound. The point is that the more deeply ramified the extension the slower
  $\nu(\varpi^k)$ grows (or the quivker $\nu(i!)$ depending on normalization).
\end{proof}
\begin{corollary}
  We have an isomorphism of topological groups:
  \[ U^{(1)} \iso \Z_p^{\deg(K/\Q_p)} \times G \]
  Where $G$ is a finite $p$-group.
\end{corollary}
\begin{proof}
  Since as modules we have $\frak{m}_K^n \iso \cl{O}_K$ and we have shown that
  $\cl{O}_K$ is a free $\Z_p$-module of rank $\deg(K/\Q_p)$ we see that:
  \[U_K^{(n)} \iso \Z_p^{\deg(K/\Q)}\]
  Moreover by Lemma 1.20. we know that $U_K^{(n)}$ is of finite index in
  $U_K^{(1)}$ hence we get an isomorphims:
  \[ U_K^{(1)}\iso U_K^{(n)}\times G \iso \Z_p^{\deg(K/\Q_p)} \times G\]
  is finite torsion. Allegedly this is also a $\Z_p$-module via
  ``exponentiation'' (??) and hence $G$ is a $p$-group.
\end{proof}
\begin{example}
  Consider the simplest case $K=\Q_p$, then for $p \neq 2$ we have $1 >
  \frac{1}{p-1}$ and hence an isomorphism:
  \[ U^{(1)}_K \iso \Z_p\]
  and thus we get:
  \[ \Q_p^{\times}\iso \underbrace{\Z}_{=p^{\Z}} \times \underbrace{\Z_p}_{=U^{(1)}_K} \times \mu_{p-1}\]
  Similarly one gets for every $K/\Q_p$ with $\kappa_K = \F_q$:
  \[ K^{\times} \iso \pi_K^{\Z}\times \cl{O}_K \times \mu_{q-1}\]
\end{example}
\begin{lemma}
  Let $x\in \Z_p$, then $x$ is a topological generator of $\Z_p$ if and only if
  $x$ is a unit.
\end{lemma}
\begin{proof}
  Since $\Z_p \iso \flim_n \Z/p^n$ we have that $x$ is a topological generator
  if and only if its residues generate the $\Z/p^n$ but this is clearly the case
  precisely if it is a unit.
\end{proof}
Combining this observation with the logarithm map we get:
\begin{proposition}
 For $p\neq 0$ the group $U^{(n)}_{\Q_p}= 1 +p^n\Z_p$ is topologically generated
 by $1+p^n$.
\end{proposition}
\begin{proof}
  We have an isomorphism of topological groups:
  \[ \rm{log}: U^{(n)}_{\Q_p} = 1+ p^n\Z_p \rar{\sim} \frak{m}^n_{\Q_p}=
    p^n\Z_p\iso \Z_p \]
  Which sends $1+p^n$ to a unit and so we are done.
\end{proof}
\subsection{Discriminant and Different}
For now let $L/K$ be an arbitrary field extension.
Recall that we had a trace map:
\[ Tr_{L/K}:L \to K\]
which is $k$-linear and acts by multiplication with $\deg(L/K)$ on $K$. Which
for $L/K$ separable can be computed as:
\[ Tr_{L/K}(x)= \sum_{\sigma: L \rari{} \bar{K}}^{} \sigma(x)\]
Furthermore for a tower of field extensions $K\subseteq F \subseteq L $ we have
that:
\[ Tr_{L/K}= Tr_{F/K}\circ Tr_{L/F}\]
\begin{lemma}
  For $L/K$ separable the symmetric bilinear form:
  \begin{align*}
 &L \times L \to K\\
    &(a,b) \mapsto Tr_{L/K}(ab)
  \end{align*}
  is non-degenerate and restricts to a map:
  \[ \cl{O}_L \times \cl{O}_L \to \cl{O}_K\]
\end{lemma}
\begin{proof}
The second part is immediate form our formula for the trace and the fact that
the embeddings into $\bar{K}$ preserve the valuation.
\end{proof}
From now on $L/K$ is a finite separable extension of degree $n$.
\begin{definition}
  Let $w_1, \dots, w_n$ be an $\cl{O}_K$-basis of $\cl{O}_L$, then:
  \[ d(w_1, \dots, w_n)=\det(Tr_{L/K}(w_iw_j))_{i,j} \]
  is an element of $\cl{O_K}$ and moreover for a different choice of basis these
  elements differ by a unit of $\cl{O}_K$. Thus:
  \[d_{L/K}:= d_{L/K}(w_1, \dots, w_n)\cl{O}_K\]
  is a well defined ideal of $\cl{O}_K$ called the \textit{discriminant} of the
  extension $L/K$. We also set:
  \[ \delta_{L/K}:= \nu_K(d_{L/K})\]
\end{definition}
\begin{remark}
 Note that $\delta_{L/K}$ determines $d_{L/K}$ since in $\cl{O}_K$
 \textit{every} ideal is of the form:
 \[\frak{a}= \frak{m}_K^i \quad \textnormal{ for some }~i \geq 0\]
\end{remark}
Recall that for an $\cl{O}_K$-submodule $M$ of $L$ we defined:
\[ M^{-1}:= \left\{x \in L \mid xm \in \cl{O}_K~\forall m \in M\right\}_{} \iso M^{\vee}\]
In particular if $M\iso x\cl{O}_K$ for some $x\in L$ we have:
\[ M^{-1} = x^{-1}\cl{O}_K \]
No we can give a similar construction using the trace-form, set:
\[M^{\pt}:= \left\{x \in L \mid Tr_{L/K}(xm)\in \cl{O}_K\right\}_{} \iso M^{\vee}\]
Observe that $\cl{O}_L^{-1}= \cl{O}_L$ and $\cl{O}_{L}\subseteq\cl{O}_L^{\pt}$
hence we get that:
\[ (\cl{O}_L^{\pt})^{-1} \subseteq \cl{O}_L^{-1}= \cl{O}_L\]
is an ideal in $\cl{O}_L$.
\begin{definition}
  With the notation above we call:
  \[ \cl{D}_{L/K} := (\cl{O}_{L/K}^{\pt})^{-1}\]
  the \textit{different} (ideal) of $L/K$. Again we denote the valuation:
  \[ \partial_{L/K}:= \nu_L(\cl{D}_{L/K})\]
  which determines the different.
\end{definition}
\begin{proposition}
  We have that:
  \[ \delta_{L/K}=f(L/K)\partial_{L/K}\]
  in particular if $L/K$ is totally ramified the different and discriminant agree.
\end{proposition}
Thus in the local case the different almost agree. Moreover one can interpret
this as saying that the different is ``better'' for studying the ramification
since the different gets a contribution from the inertia.
\begin{lemma}
  For a tower of finite separable extensions we have that:
  \begin{enumerate}
  \item $\partial_{L/K}= \partial_{L/F} + e(L/F)\partial_{F/K}$
    \item $\delta_{L/K} =f(F/K)\delta_{L/F} + \deg(L/F)\delta_{F/K}$
  \end{enumerate}
\end{lemma}
\begin{proof}
$1. \implies 2.$ is easy, hence it suffices to show 1. Indeed from the
transitivity of the trace on can show that:
\[ \cl{O}_L^{\pt} \iso \underbrace{\pi_F^{-\partial_{F/K}}}_{= \pi_L^{-e(F/F)\partial_{F/K}}}\pi_L^{-\partial_{L/F}}\cl{O}_L\]
and the claim follows.
  \end{proof}
  If $\cl{O}_L$ admits a power basis we have a very simple way to compute the
different:
\begin{lemma}\label{diffcomp}
If $L=K(\alpha)$ is of degree $n$ such that $1, \alpha, \dots, \alpha^{n-1}$
form an $\cl{O}_K$-basis of $\cl{O}_L$, then we have for the minimal polynomail
$f$ that:
\[ \partial_{L/K} = \nu_L(f\p(\alpha))\]
\end{lemma}
Combining these observations gives us a recipe for computing the different:\\
Given some separable $L/K$ first consider the inertia subfield $K \subseteq L_0
\subseteq L$. Since by definition $L_0/K$ is unramified our formula for a tower
of field extensions yields:
\[ \partial_{L/K}= \partial_{L/L_0}\]
However now $L/L_0$ is totally ramified by Lemma \ref{monogenic} we are in the
situation of \ref{diffcomp} and can compute the different as such.\\\\
We already saw that the unramified extensions are very simple and completely
controlled by what's going on on the level of residue fields. The ramified
extensions are much more complicated, but we can give a classification of sorts:
\begin{definition}
  A polynomial
  \[ X^n + a_{n-1}X^{n-1}+ \dots + a_1X + a_0 \in \cl{O}_K[X]\]
  is called \textit{Eisenstein} if $a_i\in \frak{m}_K$ for all $i$ and $a_0\notin \frak{m}_K^2$
\end{definition}
\begin{proposition}
\begin{enumerate}
\item If $f\in \cl{O}_K[X]$ is Eisenstein then it is irreducible and for any
  $\pi$ such that $f(\pi)=0$ the extension $K(\pi)/K$ is totally ramified with
  prime element $\pi \in \cl{O}_K$
  \item Conversely if $L/K$ is totally ramified then the minimal polynomial of
    $\pi_L$ is Eisenstein and we have $L= K(\pi_L)$.
\end{enumerate}
\end{proposition}
\begin{example}
  The simplest Eisenstein polynomial is given by:
  \[f= X^n - \pi_K \in \cl{O}_K[X]\]
  and hast the effect of adjoining an $n$-th root of $\pi_K$ which becomes the
  new prime element i.e.~we have a canonical choice of prime for $L= K[X]/f$
  satisfying:
  \[ \pi_L^n=\pi_K\]
  Note that we always have:
  \[ \frak{m}_L^n = \frak{m}_K\cl{O}_L\]
  but in general you cannot make a choice of generator to get this equality on
  the level of primes. However this \textit{does} work for a certain
  class extensions:
\end{example}
\begin{definition}
The extension $L/K$ is called \textit{tamely ramified} if $\ch(\kappa_K)\nmid e(L/K)$ 
\end{definition}
A counterexample is given by adjoining a $p$-th root of unity to $\Q_p$ and we
will further investigate these kinds of extensions later. Instead of giving an
example we provide a whole classification:
\begin{proposition}
Let $L/K$ be totally and tamely ramified of degree $n$, then we can choose primes
$\pi_L,\pi_K$ such that:
\[ \pi_L^n=\pi_K\]
In particular $L=K(\pi_L)$ with minimal polynomial $X^n-\pi_K$
\end{proposition}
Note that the choice of words is important, this does not hold for any pair of
primes! Consider for example the extension:
\[\Q_5(\sqrt{2\cdot 5})/\Q_5\]
Since 2 is a unit in $\Q_5$ we can pick $2\dot 5$ as a prime of $\Q_5$ and get
the desired equation.
\begin{proposition}
  We have that:
 \begin{enumerate}
 \item $e(L/K)- \leq \partial_{L/K}\leq e(L/K)-1 + \nu(e(L/K))$
   \item $\partial_{L/K}= e(L/K)-1$ if and only if $L/K$ is tamely ramified.
 \end{enumerate}
\end{proposition}
\begin{proof}
  Part 2. actually follows immediately using our classification of tamely ramified
  extensions and Lemma \ref{diffcomp} to compute the discriminant. (At least the
  ``if'' part)
\end{proof}
\subsection{Ramification Theory}
We want to study ramification phenomena more closely using Galois Theory. Thus
let $L/K$ be a finite Galois extension of local fields an write $G=G_{L/K}=
\gal(L/K)$.\\
Recall that any $\sigma \in G$ respects the valuation i.e.~we have
$\sigma(\frak{m}_L^i)=\frak{m}_L^i$ and hence $G$ acts on the quotient
$\cl{O}_L/\frak{m}^i$. 
\begin{definition}
  We put:
\[ G_i := \ker\left( G \to \aut(\cl{O}_L/\frak{m}^{i+1}) \right)\]
as the kernel of the reduction map. More explicitly this is given as the
subgroup of maps satisfying:
\[\sigma(x)\equiv x\mod\frak{m}_L^{i+1} \quad \textnormal{ for all }~x\in \cl{O}_L\]
The group $G_i$ is called the $i$-th ramification group of $L/K$.
\end{definition}
\noindent
Hence we get a descending filtration of normal subgroups:
\[ \dots \subseteq G_i \subseteq \dots \subseteq G_1 \subseteq G_0 \subseteq G\]
We want to interpret these subgroups and compute the associated graded.\\
First of all note that by definition $G_0$ sits in a short exact sequence:
\[ 1 \to G_0\to G \to \rm{Gal}(\kappa_L/\kappa_K) \to 1\]
and we've already seen that the right hand map is an isomorphism iff $L/K$ is
unramified. (Confusingly $G_0$ is also known as the \textit{inertia subgroup}).
Moreover note that for a subgroup $H \subseteq G$ we have $H_i = H \cap G_i$
thus to study the groups $G_i$ we can always assume that $L/K$ is totally ramified
i.e.~$G_0 = G$.\\
Recall that by Corollary \ref{monogenic} we have for any prime $\pi_L$ of $L$
that:
\[ \cl{O}_L = \cl{O}_K[\pi_L]\]
First some observations:
\begin{lemma}
  We have that:
  \begin{enumerate}
  \item $G_i= \{ \sigma \in G \mid \nu_L(\sigma(\pi_L)- \pi_L)>i\}$
    \item $G_i = \{1\}$ for $i \geq \max_{\sigma \in G\setminus
        \{1\}}(\nu(\sigma(\pi_L - \pi_L)))$
  \end{enumerate}
\end{lemma}
\begin{proof}
  Clear from the definitions.
\end{proof}
\begin{proposition}
  We have the following equalities:
  \[\partial_{L/K}= \sum_{1\neq \sigma \in G}^{} (\#G -1) = \sum_{1 \neq \sigma
      \in G} \nu_L(\sigma(\pi_L)-\pi_L))\]
  where the left hand side even holds if $L/K$ is not totally ramified.
  t
\end{proposition}
\begin{proof}
  We've already discussed that for $f = \prod_{\sigma \in G }(X- \sigma(\pi_L))\in \cl{O}_K[X]$ the minimal polynomial of
  $\pi_L$ we can compute the different as:
 \begin{align*}
   \partial_{L/K}= \nu_L(f\p(\pi_L))&= \nu_L\left( \prod_{1 \neq \sigma \in G}(\pi_L - \sigma(\pi_L)) \right)\\
                                    &= \sum_{1 \neq \sigma \in G} \nu(\sigma(\pi_L)-\pi_L)
 \end{align*}
 For the other equality simply observe that:
 \begin{align*}
   \sum_{1 \neq \sigma \in G} \nu(\sigma(\pi_L)-\pi_L) &= \sum_{i=0}^{\infty}i\#(G_{i-1}\setminus G_i)\\
                                                       &= \sum_{i=1}^{\infty}i \left( (\#G_{i-1}-1) - (\#G_i -1) \right)\\
   &= \sum_{i=0}^{\infty}(\# G_i - 1)
 \end{align*}
 By some telescope sum trick.
\end{proof}
Note that this equivalently $\#G-j$ where $j$ is the index such that the $G_i$
become trivial for $j > i$.\\
We want to relate the ramification groups to the higher unit groups discussed
earlier. The key observation is the following lemma:
\begin{lemma}
Let $\sigma \in G$ be any automorphism, then we have $\sigma \in G_i$ if and
only if $\frac{\sigma(\pi_L)}{\pi_L} \in U_L^{(i)}$ 
\end{lemma}
\begin{proof}
  Indeed:
  \begin{align*}
    \sigma \in G_i \iff \nu_L(\sigma(\pi_L)-\pi_L)\geq i+1 \iff \nu_L(\frac{\sigma(\pi_L)}{\pi_L}-1 \geq i)
    \iff \frac{\sigma(\pi_L)}{\pi_L}-1 \in \frak{m}_L^i
  \end{align*}
\end{proof}
\begin{proposition}
  The map:
  \begin{align*}
  G_i/G_{i+1} &\rari{} U^{(i)}_{L}/U_L^{(i+1)}\\
    \sigma &\mapsto \frac{\sigma(\pi_L)}{\pi_L}
  \end{align*}
  is a well defined injective group homomorphism which is independent of the
  choice of $\pi_L$.
\end{proposition}
\begin{proof}
  Let $\sigma \in G_i$ then by definition we have for every unit $a \in
  \cl{O}_K^{\times}$:
  \[ \sigma(a)- a \in \frak{m}^{i+1} \iff \frac{\sigma(a)}{a} -1 \in \frak{m}^{i+1}\]
  hence the map is independent of the choice of $\pi_L$. But consequently it is
  also multiplicative since $\sigma(\pi_L)$ is again a prime of $L$ and thus
  it's compatible with composition on the left hand side. Finally it's injective
  by our previous Lemma.
\end{proof}
\begin{remark}
  Note that the right hand side is completely independent of $K$ and hence this
  map cannot be an isomorphism in general.
\end{remark}
\begin{corollary}
Let $p=\ch{\kappa_K}$ and $ q= \#\kappa_L =\#\kappa_K$, then we have the
following:
\begin{enumerate}
\item $G_0/G_1$ is cyclic of order dividing $q-1$
  \item $G_i/G_{i+1}$ are abelian of an order dividing $q$ for $i \geq 1$
    \item $G_1$ is a $p$-group
      \item $G$ is solvable
\end{enumerate}
\end{corollary}
\begin{remark}
  Since without any assumptions we have:
  \[ G/G_0 \iso \gal(\kappa_L/\kappa_K)\]
  the claims 3. and 4. are true in general.
\end{remark}
\begin{corollary}
An arbitrary Galois extensions of local fields is tamely ramified if and only if
$G_1= \{1\}$
\end{corollary}
\begin{proof}
  From 1. we see that:
  \[\#G_0 = a \#G_1  \quad a \mid q-1\]
  and from 2. we get that \[\#G_1 = \#G_2 b \quad b \mid q=p^k \]
  if $b=1$ then either we already have $\#G_1 =\{1\}$ in which case $p \nmid
  \#G_0$ or we continue until the filtration jumps at say $i$ where we get:
  \[ p \mid \#G_i =\#G_1 \implies p \mid \#G_0 \]
  This shows the claim.
\end{proof}
\begin{definition}
  The group $G_1$ is called the \textit{tame ramification subgroup} for obvious reasons.
\end{definition}
\underline{Problem}: Given a tower of Galois extensions:
\[ K \subseteq F \subseteq L\]
Given the ramification groups $(G_{L/K})_i=G_i$ we simply have:
\[ (G_{F/K})_i= G_i \cap G_{F/K}\]
Of course since quotients and subobjects are natural enemies you can't play well
with both of them! Thus in general:
\[ (G_{L/F})_i\neq G_iG_{F/K}/G_{F/K}\]
Thus we introduce a different indexing of these groups to make this identity hold.
(Add the definitions and result at some point)\\
We make the following convention:\\
Set $G_{-1}=G$ and for $x\in \bb{R}_{\geq -1}$ set $G_x=G_i$ where $i$ is the
smallest integer $\geq x$. Note that if $L/K$ is totally ramified we can also write:
\[ G_x = \left\{\sigma \in G \mid \nu_l(\sigma(\pi_L)-\pi_L) \geq x+1\right\}_{}\]
Since $G=G_0$. Moreover we set:
\[[G_0:G_{-1}]:= [G:G_0]^{-1}= \frac{1}{f(L/K)}\]
Having made these conventions we define:
\[ \varphi(y)=\varphi_{L/K}(y):= \int_{0}^y \frac{1}{[G_0:G_x]}dx  \]
In particular at a non negative integer $m \in \Z_{\geq 0}$ this has value:
\[ \varphi(m)= \frac{1}{\#G_0}(\#G_1 + \dots + \#G_m)\]
growing linearly with rate $\frac{\#G_{m+1}}{\#G_0}$ on the interval $[m,m+1]$.
Some observations:
\begin{enumerate}
  \item $\varphi(0)=0$ and if $L/K$ is totally ramfiied then $\varphi(y)=y$ for
    $y\leq 0$
\item $\varphi$ is continuous, piecewise linear, strictly monotonously increasing
  and concave and in particular defines a homeomorphism $[-1, \infty]\rar{\sim} [-1,\infty]$
\end{enumerate}
We denote the inverse of $\varphi$ by $\psi$. With this convention we get the following:
\begin{definition}
  The ramification groups in \textit{upper numbering} are defined as:
    \[ G^z:= G_{\varphi(z)} \quad \textnormal{ i.e. } \quad G^{\psi(y)}=G_y\]
\end{definition}
The crucial theorem regarding this is the following:
\begin{theorem}[Herbrandt]
  Let $H \subseteq G$ be a normal subgroup, then we have:
  \[ (G/H)_{\varphi(z)}=G_zH/H \quad \textnormal{ and } \quad (G/H)^z= G^zH/H\]
  i.e.~the upper numbering is compatible with quotients.
\end{theorem}
\begin{remark}
  \begin{itemize}
    \item While the first equation \textit{does imply} the other, note that it's
      not at all immediate! In particular this asserts that:
      \[ G_zH/H = G^zH/H\]
  \end{itemize}
\end{remark}
\begin{lemma}
  We have the formula:
  \[ \partial_{L/K}= \#G_0 \int_{-1}^{\infty}( 1- \frac{1}{\$G^z})dz\]
  (The bounds of the integral are correct here, this might be where the mistake
  cam from)
\end{lemma}
\section{The $p$-cyclotomic Extension}
Fix a finite extension $K/\Q_p$. Having already classified unramified and tamely
ramified extensions we now turn our attention to the simplest case of wild ramification:
\begin{definition}
  Let $\eps_n\in \bar{K}$ be a primitive $p^n$-th root of unity. we set:
  \[ K_n:= K(\eps_n) \quad K_{\infty}:= \bigcup_{n \geq 1}^{}K_n\]
Where $K_{\infty}/K$ is called the \textit{$p$-cyclotomic extension} of $K$.
\end{definition}
\noindent
Recall that we have a natural embedding:
\[ k:G_{K_n/K}\rari{} (\Z/p^n)^{\times}\]
Which maps $\sigma \in G_{K_n/K}$ to $k(\sigma)$ where $\sigma(\eps_n)=
\eps_n^{k(\sigma)}$.
Where:
\begin{align*}
  (\Z/p^{n+1})^{\times}\iso \Z/(p-1) \times \Z/p^n \quad p \neq 2\\
  (\Z/2^{n+2})^\times \iso \Z/2 \times \Z/2^n
\end{align*}
so in any case $\#(\Z/p^n)=(p-1)p^{n-1}$\\
First let us consider the case $K=\Q_p$. Thus consider the \textit{$p^n$-th
  cyclotomic polynomial} given by:
\[ \Phi_n(X):= X^{(p-1)p^{n-1}}+ X^{(p-2)p^{n-1}}+ \dots + X^{p^{n-1}}+1 = \sum_{i=0}^{p-1}X^{ip^{n-1}}\]
Observe that $\Phi_n(\eps_n)$ is the sum over all roots of unity and hence $=0$.
Now one can check directly that $\Phi_n(1+X)$ is actually Eisenstein and hence
$\Phi_n(X), \Phi_n(1+X)$ are both irreducible (This is not so clear to me). From
this we immediately get the following facts:
\begin{proposition}
\begin{enumerate}
\item $\deg(\Q_{p,n}/\Q_p)= (p-1)p^{n-1}$
  \item The map $K:\G_{\Q_{p,n}/\Q_p}\rar{\sim} (\Z/p^n)^\times$ is an isomorphism
    \item The element $\eps_n-1$ is a prime of $\Q_{p,n}$
      \item $\Q_{p,n}/\Q_p$ is totally ramified.
\end{enumerate}
\end{proposition}
\begin{proof}
Clear.
\end{proof}
One can give an explicit description of the ramification groups in this case but
indices where the filtration jumps are rather awkward. More precisely:
\begin{proposition}
For $G=G_{\Q_{p,n}/\Q_p}$ we have:
\begin{enumerate}
\item $G_0=G$ and $G_1 = G_{\Q_{p,n}/\Q_{p,1}}$
  \item $G_i = G_{\Q_{p,n}/\Q_{p,m}}$ for $p^{m-1}\leq i < p^m$ with $1\leq m
    \leq n$
    \item $G_i = \{1\}$ for $i\geq p^{n-1}$
\end{enumerate}
\end{proposition}
\begin{corollary}
  For $1\leq m \leq n$ we have:
  \[ \partial_{\Q_{p,n}/\Q_{p,m}}= (n-m)(p^n-p^{n-1})\]
\end{corollary}

