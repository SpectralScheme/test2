\section{Examples of Galois representations}
$K= \Q_p, G= G_{\Q_p}$, $L/\Q_p$ the unramified extension of degree $m$,
$L^\times= p^\Z\times \cl{O}_L^\times$, $N:= G_L$ is normal in $G$ and $\Delta:=
\gal(L/\Q_p)=G/N$ is a cyclic group of order $m$. \\
\underline{Local Class field theory}:\\\\
There is a natural homomorphism called the \underline{reciprocity} map:
\[ \rm{rec}:L^\times \to N^{\rm{ab}}\]
which extends to a topological isomorphism:
\[\rm{rec}:p^{\widehat{\Z}}\times \cl{O}_L^\times \rar{\sim} N^{\rm{ab}}\]
It has several functorial properties of which the following is relevant for us.
For any $\sigma \in \Delta$ the diagram:
\[\begin{tikzcd}
	{L^\times} & {N^{\rm{ab}}} \\
	{L^\times} & {N^{\rm{ab}}}
	\arrow["\sigma", from=1-1, to=2-1]
	\arrow["{\rm{rec}}", from=2-1, to=2-2]
	\arrow["{\rm{rec}}"', from=1-1, to=1-2]
	\arrow[from=1-2, to=2-2]
\end{tikzcd}\]
where the right vertical map is the conjugation by a lift $\widehat{\sigma}\in
G$ of $\sigma$ which turns out to be independent of the choice.\\\\
We now start with the continuous homomorphism:
\[ \eps_m:N \to N^{\rm{ab}}\iso p^{\widehat{\Z}}\times \cl{O}^\times_L \to
  \cl{O}_L^\times \rari{}L^\times\]
By the functoriality it satisfies:
\begin{equation}
 \eps_m(\hat{\sigma}h \hat{\sigma}^{-1})= \sigma(\eps_m(h)), \quad \forall h\in N,~\sigma\in \Delta
\end{equation}
For any integer $k \neq 0$ we view the homomorphism $\eps_m^k$ as a continuous
representation of $N$ in the 1-dimensional $L$-vectorspace $L$ given by:
\begin{align*}
  N \times L &\to L\\
  (h,a) &\mapsto \eps_m^k(h)a
\end{align*}
Next we form the \textit{induced} continuous representation:
\[ \widetilde{V}_k := \rm{ind}_N^G(\eps_m^k)\]
which is an $m$-dimensional $L$-vector space defined as follows:
\[ \rm{ind}_N^G(\eps_m^k)= \left\{f: G \to L \mid
    f(gh)=\eps^k_m(h^{-1})f(g),~\forall g\in G,~h \in N \right\}\]
note that the $f$'s are automatically continous, moreover it is $m$-dimensional
since the value of $f$ is determined by it's values on the $m$ cosets. The
action is defined as:
\[g\cdot f(g\p):= f(g^{-1}g\p)\]
But $\widetilde{V}_K$ is a $G$-representation on an $L$-vector space. We want to
descend it to a $p$-adic representation of $G$.\\
Fix a generator $\sigma$ of $\Delta$. Suppose we find an automorphism
$\Sigma:\widetilde{V}_k \rar{\sim}\widetilde{V}_k$ which satisfies:
\begin{enumerate}
\item $\Sigma$ is semi-linear with repsect to $\sigma$ i.e.~$\Sigma(af)=\sigma(a)\sigma(f)$
  \item $\Sigma^m=\id$
    \item $\sigma$ commutes with the $G$-action i.e. $\Sigma(g\cdot f)=g\cdot
      (\sigma(f))$ for all $f\in\widetilde{V}_k,~g\in G$
\end{enumerate}
By $(1)$ and $(2)$ the powers of $\Sigma$ define a semi-linear action of
$\Delta$ on $\widetilde{V}_k$. Define the $\Q_p$-vector space:
\[ V_k:= (\widetilde{V}_k)^\Delta\]
By (3) the $V_k$-action respects $V_k$, resulting in and $p$-adic
$G$-representation. Moreover by Hilbert 90 we have $L \otimes_{\Q_p}V_k=
\widetilde{V}_k$; in particular $\dim_{\Q_p}V_k = \dim_L\widetilde{V}_k=m$.
Hence $V_k$ is an $m$-dimensional $p$-adic representation of $G$.\\
For a candidate for $\sigma$ we fix some $c\in L^\times$ and we try the
definition:
\[ \Sigma(f)(g):= c\sigma(f(f(\hat{\sigma})))\]
\begin{itemize}
\item Need to check that this is well defined: For $h \in N$ we compute:
\begin{align*}
  \Sigma(f)(gh)=c\sigma(f(gh\hat{\sigma}))&= c \sigma(f(g\hat{\sigma}\hat{\sigma}^{-1}h\hat{\sigma}))\\
                                          &= c \sigma(\eps^k_m(\hat{\sigma}^{-1}h^{-1}\hat{\sigma})f(g\hat{\sigma}))\\
                                          &= \sigma(\eps_m^k(\hat{\sigma}^{-1}h^{-1}\hat{\sigma})) \Sigma(f)(g)\\
  &=^{(1)} \eps_m^k(h^{-1})\Sigma(f)(g)
\end{align*}
Hence $\Sigma$ is an automorphism of $\widetilde{V}_k$.
\item $\Sigma$ is semilinear:
  \begin{align*}
    \Sigma(af)(g)=c\sigma(af(g\hat{\sigma})) = \sigma(a) c \sigma(f(g\hat{\sigma}))= \sigma(a)\Sigma(f)(g)
  \end{align*}
\item $\Sigma$ commutes with the $G$-action:\\
  For $g, g\p \in G$ we compute;
  \begin{align*}
    \Sigma(g\cdot f)(g\p) = c\sigma(g\cdot f(g\p))&= c\sigma(f(g^{-1}g))\\
                                                  &= \Sigma(f)(g^{-1}g\p)\\
    &= g\cdot (\Sigma(f))(g\p)
  \end{align*}
\item It remains to check that:
  \begin{align*}
    \Sigma^m(f)(g) = \Sigma^{m-1}(\Sigma(f))(g)&= \Sigma^{m-1}(c\sigma(f(g\hat{\sigma})))\\
                                               &= \sigma^{m-1}(c)\Sigma^{m-2}(\Sigma(\sigma(f(g\hat{\sigma}))))\\
                                               &= \sigma^{m-1}(c)\Sigma^{m-2}(c\sigma^2(f(g\hat{\sigma}^2)))\\
                                               &= \sigma^{m-1}(c)\sigma^{m-2}(c) \Sigma^{m-2}(\sigma^2(f(g\hat{\sigma}^2)))
  \end{align*}
  inductively we get:
  \begin{align*}
    &= \sigma^{m-1}(c)\sigma^{m-2}(c)\cdots \sigma(c)c \underbrace{\sigma^m}_{=\id}(f(g\hat{\sigma}^m))\\
    &= N_{L/\Q_p}(c) f(g(\underbrace{\hat{\sigma}^m}_{\in N}))\\
    &= N_{L/\Q_p}(c)\eps_m^k(\hat{\sigma}^{-m})f(g)
  \end{align*}
  which is the identity if and only if:
  \begin{equation}
    N_{L/\Q_p}(c)= \eps_m^k(\hat{\sigma}^m)
  \end{equation}
  But:
  \[ \eps_m^k(\hat{\sigma}^m)= \eps_m^k(\hat{\sigma}\hat{\sigma}^m\hat{\sigma}^{-1})=^{(1)}=\sigma(\eps_m^k(\hat{\sigma}^m))\]
  and hence we in fact have: \[\eps_m^k(\hat{\sigma}^m)\in (\cl{O}_L^\times)^\Delta = \Z_p^\times\]
  \underline{Fact}: For unramified extensions $F\p/F$ the norm map $N_{F\p/F}:
  F^\times\pt \to F^\times$ is surjective.\\\\
  Therefore we may choose $c$ in such a way that (2) is satisfied.
\end{itemize}
Hence such a $V_k$ exists!
\begin{exercise}
Up to isomorphism $V_k$ is independent of the choice of $c$
\end{exercise}
\begin{proposition}
  $V_k$ is an irreducible representation of $G$.
\end{proposition}
\begin{proof}
  It suffices to show that:
  \[ L \otimes_{\Q_p}V_k = \widehat{V}_k = \rm{ind}_N^G(\eps_m^k)\]
  is irreducible as a $G$-representation over $A$. The map:
  \begin{align*}
    \rm{ind}_N^G(\eps_m^k)&\rar{\sim} \bigoplus_{i=1}^mL\\
   f &\mapsto (f(\hat{\sigma}^i))_i 
  \end{align*}
  is a $L$-linear isomorphism. For $k \in \bb{N}$ we compute:
  \begin{align*}
    h \cdot (f\hat{\sigma}^i) f(h^{-1}\hat{\sigma}^i)&= f(\hat{\sigma}^i \hat{\sigma}^{-i}h^{-1}\hat{\sigma}^i)\\
                                                     &= \eps_m^k(\hat{\sigma}^{-i}h\hat{\sigma}^i)f(\hat{\sigma}^i)\\
                                                     &=^{(1)} \sigma^{-i}(\eps_m^k(h))f(\hat{\sigma}^i)\\
                                                     &=(\sigma^{-i}\eps_m)^k(h)f(\hat{\sigma}^i)
  \end{align*}
  This means that  our map is an isomorphism of representations of $N$:
  \begin{align*}
    \rm{ind}_N^G(\eps_m^k)\rar{\sim} \bigoplus_{i=1}^m(\sigma^{-i}\eps_m)^k
  \end{align*}
  between the left and the right hand side the 1-dimensional representations
  given by the homomorphisms $(\sigma^{-i}\eps_m)^k$.\\
  Suppose that $(\sigma^{-i}\eps_m)^k= (\sigma^j\eps_m)^k$ then we have a
  commutative diagram:
  \[\begin{tikzcd}
	&& {\cl{O}^\times_L} & {\cl{O}_L^\times} \\
	N &&&&& {\cl{O}_L^\times} \\
	&& {\cl{O}^\times_L} & {\cl{O}^\times_L}
	\arrow[from=2-1, to=1-3]
	\arrow[from=2-1, to=3-3]
	\arrow["{\sigma^{-j}}", from=3-3, to=3-4]
	\arrow["{\sigma^{-i}}", from=1-3, to=1-4]
	\arrow[shorten <=13pt, shorten >=13pt, Rightarrow, no head, from=1-3, to=3-3]
	\arrow["{(\blank)^k}", from=1-4, to=2-6]
	\arrow["{(\blank)^k}"', from=3-4, to=2-6]
\end{tikzcd}\]
Then since $k\neq 0$ on $1 + \frak{m}_L^r$ for $r >> 0$ we have that
$\sigma^{-i}=\sigma^{-j}$, hence $\sigma^{j-i}=\id$ and so $j \equiv
i~\rm{mod}~m$\\
This shows that the 1-dimensional representations on the right hand side are
pairwise non-isomorphic. On the other hand the $G$-action permutes them
transitively.\\
Suppose $U \subseteq \rm{ind}_N^G(\eps_m^k)$ is a $G$-invariant subspace, then it
is also $N$-invariant and therefore must be given by:
\[ U \rar{\sim} \bigoplus_{i \in I_U} (\sigma^{-i}\circ \eps_m)^k, \quad
  \textnormal{ with }~I_U \subseteq \{1, \dots ,m\}\]
$G$-action ``transitive'' $\implies U= \{0\}$ or $=$ everything.
\end{proof}
\noindent
Recall that for anz $k^{(\infty)}$-representation $W$ of $\Gamma$ the Sen
operator $\varphi_W$ is the $K^{(\infty)}$-linear ma defined by:
\[ \varphi_W(w):= \frac{d}{dt} \exp_\Gamma(t)(w)|_{t=0}\]
Let $\rm{Mod}_{\rm{Sen}}(K^{(\infty)})$ denote the category of pairs
$(W,\varphi)$ where $W$ is a finite dimensional $K^{(\infty)}$-vector space and
$\varphi: W \to W$ is a $K^{(\infty)}$-linear map, with morphisms given by:
\[ \alpha: (W_1, \varphi_1)\to (W_2, \varphi_2)\]
a $K^{(\infty)}-$-linear map $\alpha: W_1 \to W_2$ such that the following
diagram commutes:
\[\begin{tikzcd}
	{W_1} & {W_2} \\
	{W_1} & {W_2}
	\arrow["\alpha", from=1-1, to=1-2]
	\arrow["{\varphi_2}", from=1-2, to=2-2]
	\arrow["{\varphi_1}"', from=1-1, to=2-1]
	\arrow["\alpha"', from=2-1, to=2-2]
\end{tikzcd}\]
 Then the pair of a $K^{(\infty)}$-representation of $\Gamma$ and its Sen operator is an object of
$\rm{Mod}_{\rm{Sen}}(K^{(\infty)})$. 
\begin{remark}
  Let $W, W_1, W_2$ be $K^{(\infty)}$-representations of $\Gamma$, then we have:
  \begin{enumerate}
  \item Any homomorphism $\alpha: W_1 \to W_2$ of $K^{(\infty)}$-representations
    induces a morphism in $\M_{\rm{Sen}}(K^{(\infty)})$ and hence we get a
    functor:
    \[ \rm{Rep}_{K^{(\infty)}}(\Gamma)\to \rm{Mod}_{\rm{Sen}}(K^{(\infty)})\]
  \item We have that for any $\gamma \in \Gamma$:
    \[ \gamma \circ \varphi_W = \varphi_w \circ \gamma\]
    \item The characteristic polynomial of $\varphi_W$ has coefficients in $K$
  \end{enumerate}
  \begin{proof}
    1. and 2. are clear from the definition of the Sen operator.\\
    Ad 3: Let $A \in M_{d\times d}(K^{(\infty)})$ be the matrix of $\varphi_W$
    with respect to a fixed basis $w_i$ of $W$ i.e.:
    \[ \varphi_W(w_j)= \sum_i a_{ij}w_i, \quad A = (a_{ij})\]
    Then for $\gamma \in \Gamma$ we have:
    \[ \sum_i\gamma(a_{ij})\gamma(w_i)= \gamma(\varphi_W(w_i))= \varphi_W(\gamma(w_j))\]
    This says that $\varphi_W$ in the basis $\gamma(w_1), \dots , \gamma(w_l)$
    has the matrix $\gamma A = (\gamma (a_{ij}))$. Hence for the characteristic
    polynomials we have:
    \[ P(\varphi_W)= P(A)= P(\gamma A)= \gamma \cdot P(A)= \gamma (\varphi_W)\]
    hence $\gamma$ fixes the coefficients of the characteristic polynomial $P(\varphi_W)$
  \end{proof}
\end{remark}
\begin{lemma}
  For any $K^{(\infty)}$-representation $W$ of $\Gamma$ the natural map:
  \[ K^{(\infty)}\otimes_K W^\Gamma\to \rm{ker}(\varphi_W)\subseteq W\]
  \[ a \otimes w \mapsto aw\]
  is well defined and an isomorphism.
\end{lemma}
\begin{proof}
  The definition of $\phi_W$ implies that $\phi_W(w)=0$ for $w \in W^\Gamma$ and
  hence the map is well defined. Moreover we've already seen the injectivity.\\
  Remark 14.1.ii says that $\ker(\varphi_W)\subseteq W$ is a
  $K^{(\infty)}$-subrepresentation of $W$. Hence by the first part of that
  remark we have a commutative diagram:
\[\begin{tikzcd}
	{\ker(\varphi_W)^\Gamma} & {W^\Gamma} \\
	{\ker(\varphi_W)} & W \\
	{\ker(\varphi_W)} & W
	\arrow[hook, from=2-1, to=2-2]
	\arrow["{\varphi_W}", from=2-2, to=3-2]
	\arrow["{\varphi_{\ker(\varphi_W))}=0}"', from=2-1, to=3-1]
	\arrow[hook, from=3-1, to=3-2]
	\arrow[from=1-2, to=2-2]
	\arrow[hook', from=1-1, to=2-1]
	\arrow[shorten <=3pt, shorten >=3pt, Rightarrow, no head, from=1-1, to=1-2]
\end{tikzcd}\]
So the surjectivity claim reduces to the surjectivity f:
\[ K^{(\infty)}\otimes_K \ker(\varphi_W)^\Gamma \to \ker(\varphi_W)\]
hence we need to consider only the case of a $W$ for which $\varphi_W= 0$. Now
with the notations of last time the $\Gamma$-invariant subspace:
\[ W_m = K^{(m)}w_1 \oplus \dots \oplus K^{(m)}w_d\]
we then had:
\[ 0=\varphi_W |_{m}= \frac{\log(\pi_m(\gamma_m))}{\log(\kappa(\gamma_m)}\]
where $\pi_m(\gamma)_m$ was the matrix of $\gamma_m$ in the basis $w_1, \dots,
w_n$. Hence we get that $\gamma_m|_{W_m}= \id$ and morerover:
\[ \overline{\langle  \bar{\gamma}_m \rangle}= \Gamma_{m(r)}\]
acts trivially on $W_m$ and hence also on $(m(r) \geq m)$ on
$K^{(m(r))}\otimes_{K^{(m)}} W_m=W_n$ for $n=m(r)$, so $\Gamma$ acts on $W_n$
through its finite quotient $\Gamma/\Gamma_n$, i.e.~the action of $\Gamma$ on
the basis $w_1, \dots, w_d$ is given by a 1-cocycle:
\[ \Gamma \to \Gamma/\Gamma_n\to \gl_d(K^{(m)})\]
hence by Hilbert 90 we can replace the $w_1, \dots, w_n$ by another basis
$w_1\p, \dots, w_d\p$ which is fixed by $\Gamma$ and hence we are done.
\end{proof}
\noindent Let $W_1, W_2$ be two $K^{(\infty)}$-representations of $\Gamma$. Then
$ \mathrm{Hom}^{}_{K^{(\infty)}}(W_1,W_2)$ is a finite dimensional
$K^{(\infty)}$-vectors space with a $\Gamma$-action givenby:
\[(\gamma\cdot f)(w_1)= \gamma(f(\gamma^{-1}w_1))\]
So $W=\hom_{K^{(\infty)}}(W_1, W_2)$ is again a $K^{(\infty)}$-reprsentation of
$\Gamma$.
Note that : \[ \mathrm{Hom}^{}_{K^{(\infty)}}(W_1,W_2)^\Gamma=
\mathrm{Hom}^{}_{\rm{Rep}_{K^{(\infty)}}(\Gamma)}(W_1,W_2) \]
The by our lemma we have that $K^{(\infty)}\otimes W^\Gamma \iso \ker(\varphi)$
where the latter is given by:
\[ \left\{f \in \mathrm{Hom}^{}_{K^{(\infty)}}(W_1,W_2) \mid \varphi_W(f)=0\right\}_{}\]
Exercise: $\varphi_W(f)=\varphi_{W_2}\circ f- f \circ \varphi_{W_1}$ follows
from the product formula for derivatives and the bilinear map $W \times W_1 \to
W_2,~(f, w_1)\mapsto f(w_1)$.\\
As a result we get:
\begin{align*}
K^{(\infty)} \otimes_K \mathrm{Hom}^{}_{\rm{Rep}_{K^{(\infty)}}(\Gamma)}(W_1,W_2)\rar{\sim} \mathrm{Hom}^{}_{\M_{\rm{Sen}}}(W_1,W_2)
\end{align*}
The functor:
\[ \rm{Rep}_{K^{(\infty)}}(\Gamma)\to \M_{\rm{Sen}}(K^{(\infty)})\]
but the above result is the next best thing.
\begin{proposition}
Let $f: W_1\to W_2$ be two $K^{(\infty)}$ representations of $\Gamma$. If the
corresponding objects in $\rm{Mod}_{\rm{Sem}}(K^{(\infty)})$ are isomorphic then
$W_1$ and $W_2$ are isomorphic as well. (i.e.~ we have an iso on $\pi_0$)
\end{proposition}
\begin{proof}
  Let $f_1:W_1 \rar{\sim} W_2$ be a $K^{(\infty)}$-linear isomorphism such that
  $\varphi_{W_2} \circ f = f \circ \varphi_{W_1}$. By the result we can wirte:
  \[ f = \sum_{i=1}^ka_i f_i, \quad a_i \in K^{(\infty)},~f_i:W_1 \to W_2~\rm{in}~\rm{Rep}_{K^{(\infty)}}(\Gamma)\]
  Now fix bases of $W_1$ and $W_2$ and let $A_i$ be the matrix of $f_i$ in these
  bases. Consider the polynomial:
  \[ P(T_1, \dots, T_k)= \det(T_1A_1 + \dots +T_kA_k)\in K^{(\infty)}[T_1, \dots
   , T_k]\]
 Then $a_1A_1 + \dots + a_k A_k$ is an invertible Matrix since it describes $f$.
 Hence we have:
 \[ P(a_1, \dots , a_n) \neq0 \implies P(T_1, \dots, T_k)\neq 0\]
 Now since $K$ is infinite there exist $b_1, \dots b_h \in K$ such that $P(b_1,
 \dots, b_k)\neq 0$. So $\sum_i b_i A_i$ describes isomorphism $ \sum_i b_i f_i:
 W_1\rar{\sim} W_2$ in $\rm{Rep}_{K^{(\infty)}}(\Gamma)$.
\end{proof}
If $W=D_{\rm{Sen}}(V)$ for some $V\in \rm{Rep}_{\Q_p}(G_K)$ then we write $\varphi_V=\varphi_W$
\begin{example}
  We view the integral powers $\kappa^i$ for $i \in \Z$ of the $p$-cyclotomic
  character $\kappa: G_K \to \Z_p^\times$ as 1-dimensional $p$-adic
  representations $V_i= \Q_p$ if $G_K$ via:
  \[ G_K \times V_i \to V_i\]
  \[ (g,a)\mapsto \kappa^i(g)a\]
Then $D_{\rm{Sen}}(V_i)$ is a 1-dimensional $K^{(\infty)}$-vector space. Hence
$\varphi_{V_i}$ si the multiplication by an element $s_i \in K$. \\
\underline{Question:} What is $s_i$?\\
The corresponding $\C_p$-representation:
\[W_i = \C_p \otimes_{\Q_p} V_i = \C_p\]
is given by:
\begin{align*}
  G_K \times \C_p &\to \C_p\\
                    (g,z) &\mapsto \kappa^i(g)g(z)=: g\pt_i z
\end{align*}
Then $D_{\rm{Sen}}(V_i)=(\C_p^{H_{K\pt_i}})$ is a 1 dimensional
$K^{(\infty)}$ subvectorspace of $C_p$. As a basis we can take any  $\varpi_i
\neq 0$ in $\C_p$ such that:
\begin{enumerate}
\item $\varpi_i \in \C_p^{H_K,\pt_i}$ i.e.~$\kappa^i(g)g(\varphi_i)= \varphi_i$
  or equivalently $g(\varphi_i)=\kappa^{-i}(g )\varpi_i$ for any $g \in H_K$
  \item $\varphi_i \in \left( \C_p^{H_K,\pt-i} \right)_f$ i.e.~the $K$-vector
    space $\langle \kappa^i(\gamma)\gamma(\varphi_i):\gamma \in \Gamma \rangle_K$
    is finite dimensional. (This expression is well defined by 1.)\\
    The $\pt_i$-action of $G_K$ on $\C_p$ induces the $\Gamma$-action on
    $D_{\rm{Sen}}(V_i) K^{(\infty)}\varpi_i$. The latter is described by a
    1-cocycle:
    \[c_i: \Gamma \to K^{(\infty)}\]
    such that:
    \[\kappa^i(\gamma)\gamma(\varpi_i)=c_i(\gamma)\varpi_i ,\quad \forall
      \gamma \in \Gamma\]
    In the construction of the Sen operator we had seen that:
    \[ \varphi_{V_i}(x)=x \frac{\log(c_i(\gamma_n))}{\log(\kappa(\gamma_n))}\]
    where $\overline{\langle \gamma_n \rangle}=\Gamma_{n(r)}\subseteq \Gamma_n$
    for any $n >> 0$.
    \begin{enumerate}
    \item[Step 1] We may take $?= \varpi_1^{i}$\\
      Indeed: We have that:
      \begin{align*}
        \kappa^i(g)g(\varpi^i_1)=(\kappa(g)g(\varpi_1))^i =\varpi_1^i
      \end{align*}
      Moreover we have:
      \[ \langle  \kappa(\gamma)\gamma(\varpi_1)= (\kappa(g)g(\varpi_1))^i =
        \varpi_1^i \rangle\]
      \[ \implies \langle \kappa^i(\gamma)\gamma(\varpi_1^i) \rangle_K =
        \oplus_r K v_r= \langle (\kappa(\gamma)\gamma(\varpi_1))^i \rangle_K\]
      where the last term is contained in the $K$-linear Hull of all $i$-fold
      products of the $v_r$. 
    \item[Step 2]Have that:
      \begin{align*}
        c_i(\gamma)&= \kappa^i(\gamma)\gamma(\varpi_1^i)\\
        &=(\kappa(\gamma)\gamma(\varpi_1))^i \\
        &=c_1(\gamma)^i\varpi_1^i
      \end{align*}
      Hence we have $c_i(\gamma)= c_1(\gamma)^i$ and so by our formula:
      \begin{align*}
        s_i = \frac{\log c_i(\gamma_n)}{\log \kappa(\gamma_n)}= \frac{\log c_1(\gamma_n)^i}{\kappa(\gamma_n)} =i \frac{\log c_1(\gamma_n)}{\log \kappa(\gamma_n)} = i s_1
      \end{align*}
      \item Consider the case $i= p-1$. We claim that in this case we also may
        take:
        \[\varpi_{p-1}=1 \in \C_p\]
        Indeed:
        \[ \kappa^{p-1}(g)g(1)=\kappa^{p-1}(g)=1, \quad \forall g \in H_K\]
        since $\kappa^{p-1}$ factors through:
\[\begin{tikzcd}
	{G_K} & {\gal(K_\infty/K)} & {\Z_p^\times} \\
	& {\Gamma = \gal(K^{(\infty)}/K)}
	\arrow[from=1-1, to=1-2]
	\arrow["{\kappa^{p-1}}", from=1-2, to=1-3]
	\arrow[from=1-2, to=2-2]
	\arrow[from=2-2, to=1-3]
\end{tikzcd}\]
Moreover we have:
\[ \langle \kappa^{p-1}(\gamma)\gamma(1):\gamma \in \Gamma \rangle_K = \langle
  \kappa^{p-1}(\gamma):\gamma \in \Gamma \rangle_K = K\]
The correspoinding 1-cocycle simply is:
\[ \kappa^{p-1}: \Gamma \to K^{(\infty)}\]
and hence:
\[ s_{p-1}= \frac{\log \kappa^{p-1}(\gamma_n)}{\log \kappa(\gamma_n)}=p-1\]
and alltogether we have seen that:
\[ s_i = i, \quad \forall i \in \Z\]
    \end{enumerate}
\end{enumerate}
\end{example}
\begin{example}
  The $p$-adic representations $V_k$ of $G_{\Q_p}$ satisfy:\\
  Then Sen operator $\varphi_{V_K}$ is semisimple with $m-1$ eigenvalues $0$ and
  one eigenvalue $=k$,i.e.~:
  \[D_{\rm{Sen}}(V_K)\iso \ker(\varphi_{V_k}) \oplus K^{(m)} \]
  where $\varphi_{V_k}$ acts on the right hand term by $k$.
\end{example}
\begin{definition}
 A $p$-adic Galois representation $V$ of $G_K$ is called \textit{Hodge-Tate} if
 $\varphi_V$ is semisimple with all eigenvalues $\in \Z$
\end{definition}
\begin{theorem}
  For a $p$-adic representation $V$ of $G_K$ the following are equivalent:
  \begin{enumerate}
  \item $\varphi_V=0$
  \item $V$ is called $\C_p$-admissible,i.e.:
    \[ \C_p \otimes_{\Q_p}V = \C_p \otimes_K (\C_p \otimes_{\Q_p}V)^{G_K}\]
    \item Some open subgroup of $I_K$ acts trivially on $V$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  We only show the easy part $1. \iff 2.$
  we compute that:
  \[ (\C_p \otimes_{\Q_p}V)^{G_K}= ((\C_p \otimes_{\Q_p} V)^{H_K})^\Gamma= (((\C_p
    \otimes_{\Q_p}V)^{H_K})_f)^{\Gamma}= D_{\rm{Sen}}(V)^{\Gamma}\]
  Tensoring back with $\C_p$ gives:
  \begin{align*}
    \C_p \otimes_K (\C_p \otimes_{\Q_p}V)^{G_K} &= \C_p \otimes_K D_{\rm{Sen}}(V)^{\Gamma}\\
                                                &= \C_p \otimes_{K^{\infty}}(K^{(\infty)}\otimes_K D_{\rm{Sen}}(V)^{\Gamma})\\
                                                &= \C_p \otimes_{K^{(\infty)}} \ker(\varphi_V)\\
                                                &\subseteq \C_p \otimes_{K^{(\infty)}} D_{\rm{Sen}}(V) = \C_p \otimes_{\Q_p}V
  \end{align*}
  Obviously this is an equality if and only if $\varphi_V = 0$\\
  The implication $3 \implies 1$ is also not so hard with our tools, but $1
  \implies 3$ requires considerably more theory.
\end{proof}
\noindent
\underline{Complement}:\\
Recall the ring $B_{HT}:= \C_p[t^{\pm}]$ with semi linear action induced by $g(t^i)=\kappa^i(g)t^i$
\begin{proposition}
  A $p$-adic representation $V$ of $G_K$ is Hodge-Tate if and only if $V$ is $B_{HT}$-admissible.
\end{proposition}
The concept of admissibilty (Fontaine:)\\\\
Let $B$ be a topological $\Q_p$-algebra which:
\begin{itemize}
\item is an integral domain
\item is equipped with a continuous $G_k$-action
  \item $E:= B^{G_K}$ is a field (containing $\Q_p$)
\end{itemize}
    Then as before we set $\rm{Rep}_{B}(G_K)$ the category of all finitely
    generated free $B$-modules with a semilinear continuous $G_K$-action.
    \begin{definition}
      $W$ in $\rm{Rep}_B(G_K)$ is called \textit{trivial or induced} if:
      \[ W\iso B \otimes_E X\]
      for some $E$-vectors space $X$.
    \end{definition}
    Then for any $V$ in $\rm{Rep}_{\Q_p}(G_K)$ we may form the $E$-vector space:
    \[ D_B(V):= (B \otimes_{\Q_p}V)^{G_K}\]
    \begin{definition}
      $V$ is called $B$-\textit{admissible} if $B \otimes_{\Q_p}V$ is trivial.
    \end{definition}
    \begin{remark}
      We always have the natural $G_K$-equivariant map:
     \begin{align*}
       \alpha_V: B \otimes_E D_B(V)&\to B \otimes_{\Q_P}v\\
       b\otimes x &\mapsto bx
     \end{align*}
     And we have seen that under mild additional assumptions on $B$ the
     following are equivalent:
     \begin{enumerate}
     \item $V$ is $B$-admissible
       \item $\alpha_V$ is bijective
         \item $\dim_{E}D_B(V) = \dim_{\Q_p}V$
     \end{enumerate}
     Such rings $B$ are:
     \[ \bar{K}, \C_p, B_{HT}, B_{dR}, B_{sst}, B_{crys}\]
     But Sen theory does not fall under this concept!\\
     Things become more interesting if $B$ has some additional structure which
     is respected by the $G_K$-action.
    \end{remark}
    \noindent
    \underline{The case $B_{cris}$ for $K=\Q_p$}:
    \begin{enumerate}
    \item $B_{cris}$ is an $\widehat{\Q_p^{\rm{un}}}$-algebra and it's
      $G_{\Q_p}$-action induces on $\Q_p^{\rm{un}}$ the usual Galois action. It
      satifies:
      \[(1)\quad B_{cris}^{G_{\Q_p}}= \Q_p\]
      \item $B_{cris}$ carries a descending filtration by $\Q_p$-subvector
        spaces:
        \[ \dots \subseteq F^1B_{cris}\subseteq F^0B \subseteq F^{-1}B
          \subseteq \dots\]
        such that the union is everything and the intersection is zero.
        \item $B$ carries an endomorphism $\varphi$ which is injective (but not
          surjective) and restricts on $\Q_p^{\rm{un}}$ to the Galois Frobenius.
          It satisfies:
          \[(2) \quad B^{\varphi = \id} \cap F^0B = \Q_p\]
          \underline{Warning}: $\varphi$ does not respect the filtration.
          \item (3) Both the filtration and the map $\varphi$ are
            $G_{\Q_p}$-invariant. 
    \end{enumerate}
    Then again for $V\in \rm{Rep}(G_{\Q_p})$ we put:
    \[ D_{cris}(V):= (B \otimes_{\Q_p}V)^{G_{\Q_p}}\]
    By (1) $D_{cris}(V)$ is a $\Q_p$-vector space and moreover
    $\dim_{\Q_p}D_{cris}(V)\leq \dim_{\Q_p}V$. Form the proposition we know that
    equality holds iff $V$ is $B$-admissible.
    \begin{definition}
      $V$ is called \textit{crystalline} if it is $B_{cris}$-admissible.
    \end{definition}
    By (3), $D_{cris}(V)$ carries:
    \begin{itemize}
     \item a descending filtration:
      \[ F^iD_{cris}(V):= (F^i B_{cris}\otimes_{\Q_p}V)^{G_{\Q_p}}\]
    \item An injective $\Q_p$-linear endomorphism:
      \[ \varphi = (\varphi \otimes \id)|_{D_{cris}(V)}\]
      By our dimension estimate this filtration has only finitely many steps and
      this $\varphi$ is bijective.\\
      Suppose now that $V$ is crystalline, then we have the iso:
      \[ B_{cris} \otimes_{\Q_p}D_B(V)\rar{\sim}V \otimes_{\Q_p}B_{cris}\]
      Then on both sides we have the $G_{\Q_p}$-action, a filtration and the
      Frobenii  which correspond to each other under this iso.\\
      Left hand side:
      \begin{align*}
        (F^0(B_{cris}\otimes_{\Q_p}V))^{\varphi \otimes \id = \id} &= (F^0B_{cris})^{\varphi =\id}\otimes_{\Q_p} B\\
                                                                   &=\Q_p \otimes_{\Q_p}B = V
      \end{align*}
      ON the right hand side we also must have:
      \[(F^0(B \otimes _{\Q_p}D_B(V)))^{\varphi \otimes \varphi = \id}=V\]
      as $G_{\Q_p}$ -representations.\\
      Hence we see:\\
      The $G_{\Q_p}$-representation $V$ can be reconstructed from the triple
      $D_{cris}(V), F^i, \varphi$
    \end{itemize}
    \underline{By the way}: Any crystalline representation is Hodge-Tate and the
    eigenvalues of the Sen operator are the integers where the filration
    jumps.\\
    \underline{Further Questions:}
    \begin{itemize}
    \item Which triples come from a representation $V$?\\
      Answer: Those which satisfy an arithmetic condition on the relation
      between the filtration and the $\varphi$
    \item Which $V$ are crystalline ? \\
      Answer: Most $V$ coming from geometry are crystalline.
    \end{itemize}

